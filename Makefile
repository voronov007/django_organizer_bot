# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)


TARGET_MAX_CHAR_NUM=20
## show help
help:
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
	@echo ''

## Cleanup the codebase from temp files
clean:
	@find . -name '*.pyc' | xargs rm -rf
	@find . -name '*.egg-info' | xargs rm -rf

## Install back-end requirements
requirements-dev:
	@pip install --upgrade pip
	@pip install --upgrade poetry
	@poetry install

## run web server
run:
	poetry run python src/manage.py migrate
	poetry run python src/manage.py runserver

## run tests
test:
	poetry run pytest tests/ --cov=telegram_bots

## check tests coverage
coverage:
	poetry run pytest tests/ --cov=telegram_bots --cov-report html

## open Django shell
shell:
	python src/manage.py shell

## create migrations files
migrations:
	poetry run python src/manage.py makemigrations

## apply migrations
migrate:
	poetry run python src/manage.py migrate

## code refactoring based on PEP-8 rules and best practices
fmt:
	@poetry run isort src/telegram_bots tests
	@poetry run black -l 99 -S --target-version=py311 src/telegram_bots tests

## check if code fits PEP-8 rules and best practices
lint:
	@poetry run flake8 src/telegram_bots/ tests/
	@poetry run black -l 99 -S --target-version=py311 --check src/telegram_bots tests

## Deploy on heroku
deploy-heroku:
	git push heroku master

## dump all test data from the DB. Output - "test.json" file
dump-test:
	python src/manage.py dumpdata --exclude auth.permission --exclude contenttypes --exclude admin --exclude sessions > fixtures/test.json

## load data from the DB dump file. Args: <filename>
load_data:
	python src/manage.py loaddata fixtures/${filename}

## create translations
translations:
	python src/manage.py makemessages --locale=ua --locale=ru --ignore=venv

## compile translations
compile:
	python src/manage.py compilemessages

## run docker (after update)
docker-run:
	docker-compose up --build -d

## docker db, drop db
docker-drop-db:
	docker-compose exec db bash -c "pg_restore --verbose --clean --no-acl --no-owner -U postgres -d telegram_bots_db /var/lib/postgresql/data/latest.dump"
