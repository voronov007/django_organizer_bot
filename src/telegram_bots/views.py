from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.views.generic.base import TemplateView


class AboutView(TemplateView):
    """Monthly chart, US region."""

    template_name = "about.html"

    def get_context_data(self, **kwargs) -> dict:
        """Get context data."""
        ctx = super().get_context_data(**kwargs)
        ctx["top_yt_video_charts"] = mark_safe(  # noqa: S308
            """
            <strong class='text-solid'> Top YouTube Video Charts </strong>
            """
        )
        ctx["yt_music_videos"] = mark_safe(  # noqa: S308
            """
            <strong class="text-solid"> YouTube Music Videos </strong>
            """
        )
        ctx["title"] = "About"
        ctx["telegram_channel"] = mark_safe(  # noqa: S308
            """
            <a href="https://t.me/dude007_channel"
               class="link-solid" rel="noreferrer" target="_blank"
               aria-label="Telegram Channel">
                Telegram Channel <i class="fab fa-telegram-plane"></i>
                <i class="fas fa-external-link-alt"></i>
            </a>
            """
        )
        ctx["weekly_top_video_chart"] = mark_safe(  # noqa: S308, S703
            """
            <strong class="text-solid">Weekly Top Video Chart</strong>.
            """
        )
        href_text = f"""href="{reverse('charts:top-all-time-music-videos')}" """  # noqa: B907
        ctx["all_time_music_videos"] = mark_safe(  # noqa: S308, S703
            f"""
            <a {href_text}
               class="link-solid">
                All Time Music Videos <i class="fas fa-link"></i>
            </a>
            """
        )
        ctx["pwa"] = mark_safe(  # noqa: S308, S703
            """
            <strong class="text-solid">
            Progressive Web Application(PWA)
            </strong>
            """
        )
        ctx["olha_linkedin"] = mark_safe(  # noqa: S308, S703
            """
            <a href="http://www.linkedin.com/in/olha-voronova/"
               class="link-solid"
               rel="noreferrer"
               target="_blank"
               aria-label="LinkedIn">
                Olha <i class="fab fa-linkedin"></i>
                <i class="fas fa-external-link-alt"></i>
            </a>
            """
        )
        href_text = f"""href="{reverse('charts:index')}" """  # noqa: B907
        ctx["yt_video_charts_link"] = mark_safe(  # noqa: S308, S703
            f"""
            <a {href_text}
               class="link-solid">
                YouTube Video Charts <i class="fas fa-link"></i>
            </a>
            """
        )
        ctx["linkedin_url"] = mark_safe(  # noqa: S308, S703
            """
            <a href="https://www.linkedin.com/in/anvoronov/?locale=en_US"
               target="_blank"
               class="link-solid">
                LinkedIn <i class="fab fa-linkedin"></i>
                <i class="fas fa-external-link-alt"></i>
            </a>
            """
        )
        ctx["instagram_url"] = mark_safe(  # noqa: S308, S703
            """
            <a href="https://www.instagram.com/voronov.a.g/"
               rel="noreferrer"
               target="_blank"
               aria-label="Instagram"
               class="link-solid">
                Instagram <i class="fab fa-instagram"></i>
                <i class="fas fa-external-link-alt"></i>
            </a>
            """
        )
        return ctx


def offline(request) -> HttpResponse:
    """Build offline page."""
    data = {
        "title": "Top Charts Offline",
        "h1": "Top Charts Offline Mode",
        "text": (
            "You are viewing web site in offline mode. "
            "Videos aren't accessible without internet connection. "
        ),
        "meta_description": ("Top chart of YouTube videos. Enjoy top videos which are InMyTop. "),
        "meta_keywords": "YouTube, videos, top chart, music, movies trailers",
    }
    return render(request, "offline.html", data)
