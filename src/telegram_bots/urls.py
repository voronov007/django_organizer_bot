from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import include, path
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from telegram_bots.apps.teams_organizer.views import TeamsOrganizerView
from telegram_bots.apps.top_charts import urls as top_chart_urls
from telegram_bots.apps.top_charts.views import TopChartBotView
from telegram_bots.sitemaps import DynamicViewSitemap, StaticViewSitemap
from telegram_bots.views import AboutView, offline

sitemaps = {"static": StaticViewSitemap, "dynamic": DynamicViewSitemap}

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "webhook/team/team_organizer_bot_007/",
        csrf_exempt(TeamsOrganizerView.as_view()),
    ),
    path(
        "webhook/charts/top_chart_bot_007/",
        csrf_exempt(TopChartBotView.as_view()),
    ),
    path(
        "sitemap.xml",
        sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    path(
        "sw.js",
        TemplateView.as_view(template_name="sw.js", content_type="application/javascript"),
        name="sw.js",
    ),
    path("about/", cache_page(5)(AboutView.as_view()), name="about"),
    path("offline/", offline, name="offline"),
    path("", include(top_chart_urls, namespace="charts")),
]
