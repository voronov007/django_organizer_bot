from django.db import migrations
from django.db.models import Prefetch
from telegram_bots.apps.top_charts.utils.statistics import convert_statistics_to_str


def gen_rank(apps, schema_editor):
    YouTubeChart = apps.get_model("top_charts", "YouTubeChart")
    YouTubeVideo = apps.get_model("top_charts", "YouTubeVideo")
    charts = YouTubeChart.objects.prefetch_related(
        Prefetch("videos", queryset=YouTubeVideo.objects.order_by("position"))
    ).all()

    for chart in charts:
        videos = chart.videos.all()
        for v in videos:
            v.rank = v.vpd + (v.likes_num - v.dislikes_num) * 5
            v.rank_str = convert_statistics_to_str(v.rank)
            v.vpd_str = convert_statistics_to_str(v.vpd)
            v.save(update_fields=["rank", "rank_str", "vpd_str"])

    updated_charts = YouTubeChart.objects.prefetch_related(
        Prefetch("videos", queryset=YouTubeVideo.objects.order_by("-rank"))
    ).all()
    for chart in updated_charts:
        videos = chart.videos.all()
        for counter, v in enumerate(videos):
            v.position = counter + 1
            v.save(update_fields=["position"])


class Migration(migrations.Migration):
    dependencies = [("top_charts", "0004_auto_20190829_0841")]

    operations = [migrations.RunPython(gen_rank, reverse_code=migrations.RunPython.noop)]
