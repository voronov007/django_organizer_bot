from django.contrib import admin
from telegram_bots.apps.top_charts.models import (
    TelegramCallbackMessage,
    TelegramCallbackUserReaction,
    TopChartTelegramChats,
    YouTubeChart,
    YouTubeVideo,
)


class ChartTypeFilter(admin.SimpleListFilter):
    """Chart Type Video Admin List Filter."""

    title = "chart type"

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "chart_type"

    def lookups(self, request, model_admin) -> list[tuple]:
        """List filter lookups."""
        return [(el[0], el[1]) for el in YouTubeChart.CHART_TYPE_CHOICES]

    def queryset(self, request, queryset):
        """Queryset filter.

        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        value = self.value()
        if not value:
            return queryset
        return queryset.filter(chart__chart_type=self.value())


class MusicVideoChartAdmin(admin.ModelAdmin):
    """Music Video Chart Admin class."""

    list_display = ("id", "chart_type", "name", "date", "get_num_videos")
    list_display_links = list_display

    def get_num_videos(self, obj) -> int:
        """Get number of videos."""
        return obj.videos.count()

    get_num_videos.short_description = "Videos"


class MusicVideoAdmin(admin.ModelAdmin):
    """Music Video Admin class."""

    list_display = (
        "id",
        "position",
        "get_chart_name",
        "get_chart_type",
        "get_chart_id",
        "url",
    )
    list_display_links = list_display
    ordering = ("id",)
    search_fields = ("get_chart_id", "get_chart_type")
    list_filter = (ChartTypeFilter,)

    def get_chart_type(self, obj) -> str:
        """Get type of the chart."""
        return obj.chart.chart_type

    get_chart_type.short_description = "chart type"

    def get_chart_id(self, obj) -> int:
        """Get chart ID."""
        return obj.chart.id

    get_chart_id.short_description = "chart ID"

    def get_chart_name(self, obj) -> str:
        """Get chart name."""
        return obj.chart.name

    get_chart_name.short_description = "chart name"


class TopChartTelegramChatsAdmin(admin.ModelAdmin):
    """Admin class for Top Chart Telegram Chat."""

    list_display = ("id", "chat_id", "is_active")
    search_fields = ("chat_id",)
    list_display_links = list_display


class TelegramCallbackMessageAdmin(admin.ModelAdmin):
    """Admin class for callback message."""

    list_display = ("id", "t_msg_id", "date_created")
    list_display_links = list_display


class TelegramCallbackUserReactionAdmin(admin.ModelAdmin):
    """Admin class for callback reaction."""

    list_display = ("id", "user_id", "value")
    list_display_links = list_display


admin.site.register(TelegramCallbackMessage, TelegramCallbackMessageAdmin)
admin.site.register(TelegramCallbackUserReaction, TelegramCallbackUserReactionAdmin)
admin.site.register(YouTubeChart, MusicVideoChartAdmin)
admin.site.register(TopChartTelegramChats, TopChartTelegramChatsAdmin)
admin.site.register(YouTubeVideo, MusicVideoAdmin)
