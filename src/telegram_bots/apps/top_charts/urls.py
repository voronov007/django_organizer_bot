from django.urls import path
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from telegram_bots.apps.top_charts.views import (
    all_time_music_video_chart_view,
    PeriodicMonthlyChartTaskView,
    PeriodicWeeklyChartTaskView,
    trailers_chart_view,
    ww_music_video_chart_view,
    ww_ua_and_ru_video_chart_view,
)

# from telegram_bots.apps.top_charts.views import (
#     ATMusicVideoChartView,
#     RUMusicVideoChartView,
#     TrailersChartView,
#     WWMusicVideoChartView,
# )

app_name = "top-charts"

urlpatterns = [
    # path("", cache_page(60)(WWMusicVideoChartView.as_view()), name="index"),
    path("", cache_page(60)(ww_music_video_chart_view), name="index"),
    path(
        "top-ru-music-videos/",
        cache_page(60)(ww_ua_and_ru_video_chart_view),
        name="top-ru-music-videos",
    ),
    # path(
    #     "top-all-time-music-videos/",
    #     cache_page(60)(ATMusicVideoChartView.as_view()),
    #     name="top-all-time-music-videos",
    # ),
    path(
        "top-all-time-music-videos/",
        cache_page(60)(all_time_music_video_chart_view),
        name="top-all-time-music-videos",
    ),
    # path(
    #     "top-movie-trailers/",
    #     cache_page(60)(TrailersChartView.as_view()),
    #     name="top-movie-trailers",
    # ),
    path(
        "top-movie-trailers/",
        cache_page(60)(trailers_chart_view),
        name="top-movie-trailers",
    ),
    path(
        "periodic-task/",
        csrf_exempt(PeriodicMonthlyChartTaskView.as_view()),
        name="periodic-monthly-task",
    ),
    path(
        "periodic-weekly-task/",
        csrf_exempt(PeriodicWeeklyChartTaskView.as_view()),
        name="periodic-weekly-task",
    ),
]
