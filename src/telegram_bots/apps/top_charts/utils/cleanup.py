from telegram_bots.apps.top_charts.models import YouTubeChart

__all__ = ["clean_up_charts"]


def clean_up_charts(chart_type: str):
    """Remove old charts."""
    num_of_charts_to_save = 2
    charts = (
        YouTubeChart.objects.filter(chart_type=chart_type)
        .order_by("-date")[:num_of_charts_to_save]
        .values_list("id", flat=True)
    )

    YouTubeChart.objects.filter(chart_type=chart_type).exclude(pk__in=list(charts)).delete()
