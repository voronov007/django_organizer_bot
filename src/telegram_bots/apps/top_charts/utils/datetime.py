import datetime as dt

from django.db.models import Prefetch
from django.utils import timezone as tz
from telegram_bots.apps.top_charts.models import YouTubeChart, YouTubeVideo
from telegram_bots.constants import DAYS_IN_MONTH

__all__ = [
    "last_month_video_chart",
    "month_ago_iso",
    "days_ago_iso_start",
    "days_ago_iso_end",
    "get_or_create_today_video_chart",
    "get_dates_request_params",
    "get_youtube_iso_history_date",
]


def month_ago_iso() -> str:
    """Get datetime format as a string."""
    month_ago = dt.datetime.now().date() - dt.timedelta(days=DAYS_IN_MONTH)
    return month_ago.strftime("%Y-%m-%dT%H:%M:%SZ")


def days_ago_iso_start(days: int) -> str:
    """Get datetime format as a string."""
    past_date = dt.datetime.now().date() - dt.timedelta(days=days)
    return past_date.strftime("%Y-%m-%dT%H:%M:%SZ")


def days_ago_iso_end(days: int) -> str:
    """Get datetime format as a string."""
    _date = dt.datetime.now().date() - dt.timedelta(days=days)
    past_date = dt.datetime(_date.year, _date.month, _date.day, 23, 59, 59)
    return past_date.strftime("%Y-%m-%dT%H:%M:%SZ")


def get_youtube_iso_history_date() -> str:
    """Get datetime format as a string."""
    return dt.datetime(2006, 1, 1, 0, 0, 0).strftime("%Y-%m-%dT%H:%M:%SZ")


def last_month_video_chart(chart_type: str = YouTubeChart.WORLDWIDE) -> tuple:
    """Get last month video chart."""
    today = tz.now().date()
    # usually periodic task will run updates,
    # otherwise let's make sure is more than 31 days ago was started
    month_ago = today - tz.timedelta(days=DAYS_IN_MONTH + 1)
    created = False

    video_chart = (
        YouTubeChart.objects.prefetch_related(
            Prefetch("videos", queryset=YouTubeVideo.objects.order_by("position"))
        )
        .filter(date__range=[month_ago, today], chart_type=chart_type)
        .order_by("date")
        .last()
    )

    if not video_chart:
        video_chart = YouTubeChart.objects.create(
            name=today.strftime("%d %b %Y"), date=today, chart_type=chart_type
        )
        created = True

    return video_chart, created


def get_or_create_today_video_chart(chart_type: str) -> tuple:
    """Get today video chart."""
    today = tz.now().date()

    video_chart, created = YouTubeChart.objects.get_or_create(
        date=today,
        chart_type=chart_type,
        defaults={"name": today.strftime("%d %b %Y")},
    )

    return video_chart, created


def get_dates_request_params(start_date: str, end_date: str) -> dict:
    """Get dates request parameters."""
    return {"publishedAfter": start_date, "publishedBefore": end_date}
