import json
import logging
from queue import Queue
from typing import List

from django.conf import settings
from requests import get as request_get
from telegram_bots.apps.top_charts.mixins import YouTubeMusicChartMixin, YouTubeTrailerChartMixin
from telegram_bots.apps.top_charts.models import YouTubeChart
from telegram_bots.apps.top_charts.utils.statistics import start_video_statistics_thread
from telegram_bots.constants import (
    ALL_TIME_MUSIC_REQUEST_DATA,
    RU_MUSIC_REQUEST_DATA,
    TRAILERS_REQUEST_DATA,
    WW_MUSIC_REQUEST_DATA,
    YOUTUBE_API_URL,
)

__all__ = [
    "get_chart",
    "get_all_time_chart",
    "get_chart_request_parameters",
    "filter_result_by_threshold",
    "sort_list_by_rank",
]


logger = logging.getLogger(__name__)


def get_chart(
    query_params: dict,
    result: List[dict],
    output_queue: Queue,
    booster_rank: bool = False,
):
    """Get YouTube chart from YouTube."""
    q: Queue = Queue()
    url = YOUTUBE_API_URL.format(method="search")
    _ = output_queue.get()

    request_params: dict = {"url": url, "params": query_params}
    if settings.DEBUG:
        request_params["verify"] = False

    try:
        r = request_get(**request_params, timeout=5)
    except Exception as e:
        logger.error(f"Exception occured: {e}")
        output_queue.task_done()
        return

    if r.status_code != 200:
        logger.error("Bad response code in get chart. Trying to get details...")
        try:
            logger.error(
                {
                    "msg": "Cannot get daily chart",
                    "error": json.loads(r.content),
                    "status_code": r.status_code,
                    "text": r.text,
                }
            )
        except Exception as e:
            logger.error(
                {
                    "msg": "Cannot get daily chart error content",
                    "error": e,
                    "status_code": r.status_code,
                    "text": r.text,
                }
            )
        finally:
            output_queue.task_done()
        return

    data = json.loads(r.content)
    start_video_statistics_thread(
        data=data,
        q=q,
        result=result,
        booster_rank=booster_rank,
    )

    q.join()
    output_queue.task_done()


def get_all_time_chart(query_params: dict, result: List[dict]):
    """Get all time YouTube chart from YouTube."""
    q: Queue = Queue()
    url = YOUTUBE_API_URL.format(method="search")

    request_params: dict = {"url": url, "params": query_params}
    if settings.DEBUG:
        request_params["verify"] = False

    try:
        r = request_get(**request_params, timeout=5)
    except Exception as e:
        logger.error(f"Exception during the request: {e}")
        return

    if r.status_code != 200:
        logger.error("Bad response code in get all time chart. Trying to get details...")
        try:
            logger.error(
                {
                    "msg": "Cannot get all time chart",
                    "error": json.loads(r.content),
                    "status_code": r.status_code,
                    "text": r.text,
                }
            )
        except Exception as e:
            logger.error(
                {
                    "msg": "Cannot get all time chart error content",
                    "error": e,
                    "status_code": r.status_code,
                    "text": r.text,
                }
            )
        return

    data = json.loads(r.content)
    start_video_statistics_thread(data=data, q=q, result=result)

    q.join()


def get_chart_request_parameters(chart_type: str) -> dict:
    """Get YouTube Chart request parameters."""
    if chart_type == YouTubeChart.WORLDWIDE:
        return {
            **YouTubeMusicChartMixin.base_request_data,
            **WW_MUSIC_REQUEST_DATA,
        }
    elif chart_type == YouTubeChart.RU:
        return {
            **YouTubeMusicChartMixin.base_request_data,
            **RU_MUSIC_REQUEST_DATA,
        }
    elif chart_type == YouTubeChart.ALL_TIME:
        return {
            **YouTubeMusicChartMixin.base_request_data,
            **ALL_TIME_MUSIC_REQUEST_DATA,
        }
    elif chart_type == YouTubeChart.TRAILER:
        return {
            **YouTubeTrailerChartMixin.base_request_data,
            **TRAILERS_REQUEST_DATA,
        }

    return {}


def filter_result_by_threshold(result: List[dict], threshold: int) -> list:
    """Filter result by the threshold."""
    return [el for el in result if el["statistics"]["views_count"] >= threshold]


def sort_list_by_rank(result_list: List) -> list:
    """Sort list of elements by the rank field."""
    return sorted(result_list, key=lambda x: x["rank"], reverse=True)
