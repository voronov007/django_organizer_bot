from django.contrib.auth import authenticate
from telegram_bots.apps.users.models import User
from telegram_bots.constants import CHART_TASK_EMAIL, CHART_TASK_USERNAME

__all__ = ["is_allowed_run_chart_task", "is_allowed_run_weekly_chart_task"]


def is_allowed_run_chart_task(data: dict) -> bool:
    """Check if user is allowed to run the task."""
    allowed_user = User.objects.filter(
        username=CHART_TASK_USERNAME, email=CHART_TASK_EMAIL
    ).first()

    incoming_user = authenticate(
        username=data.get("username", ""),
        password=data.get("password", ""),
        email=data.get("email", ""),
    )

    if any((incoming_user is None, allowed_user is None)):
        return False

    if allowed_user.id != incoming_user.id:
        return False

    return True


def is_allowed_run_weekly_chart_task(data: dict) -> bool:
    """Check if user is allowed to run the weekly chart task."""
    is_allowed_user = is_allowed_run_chart_task(data)
    if not is_allowed_user:
        return False

    chart_type = data.get("chart_type", "")
    chart_type = str(chart_type).lower()
    if chart_type != "weekly":
        return False

    return True
