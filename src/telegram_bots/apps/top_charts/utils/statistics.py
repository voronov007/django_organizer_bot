import csv
import json
import logging
import os
from datetime import date as dt_date
from datetime import datetime as dt
from queue import Queue
from threading import Thread

from bs4 import BeautifulSoup
from django.conf import settings
from requests import get as request_get
from telegram_bots.constants import (
    BOOSTER_RANK_MULTIPLIER,
    GOOGLE_API_KEY_2,
    YOUTUBE_API_URL,
    YOUTUBE_API_WATCH_URL_FORMAT,
)

__all__ = ["start_video_statistics_thread", "convert_statistics_to_str"]


logger = logging.getLogger(__name__)


class YTStatistics:
    __slots__ = ["video_id", "title", "watch_url", "day", "published_at"]

    def __init__(
        self,
        video_id: str,
        title: str,
        watch_url: str,
        day: int,
        published_at: dt_date,
    ):
        self.video_id = video_id
        self.title = title
        self.watch_url = watch_url
        self.day = day
        self.published_at = published_at


YTDeque = "Queue[YTStatistics]"


def start_video_statistics_thread(
    data: dict,
    q: Queue,
    result: list,
    booster_rank: bool = False,
):
    """Get video statistics (main function)."""
    today = dt.now().date()
    items = data["items"]
    if not items:
        logger.warning("VideoChart doesn't have any item for this day.")

    for item in items:
        video_id = item["id"]["videoId"]
        title = item["snippet"]["title"]
        published_at = item["snippet"]["publishedAt"]
        published_at = get_date_from_google_dt_string(published_at)
        day = get_days_ago_from_date(today, published_at)
        title_html = BeautifulSoup(title, "html.parser")
        q.put(
            YTStatistics(
                video_id=video_id,
                title=title_html.text,
                watch_url=YOUTUBE_API_WATCH_URL_FORMAT.format(id=video_id),
                day=day,
                published_at=published_at,
            )
        )
        worker = Thread(
            target=get_video_statistics,
            args=(q, result, booster_rank),
            daemon=False,
        )
        worker.start()


def get_video_statistics(q: YTDeque, result: list, booster_rank: bool = False):
    """Get video statistics."""
    url = YOUTUBE_API_URL.format(method="videos")
    yt_video = q.get()
    params = {
        "key": GOOGLE_API_KEY_2,
        "part": "statistics,status",
        "id": yt_video.video_id,
    }
    request_params: dict = {"url": url, "params": params}
    if settings.DEBUG:
        request_params["verify"] = False

    try:
        r = request_get(**request_params, timeout=5)
    except Exception as e:
        logger.error(f"Exception occured during video statistics get: {e}")
        q.task_done()
        return

    if r.status_code != 200:
        logger.error("Bad response code in get video statistics. Trying to get details...")
        try:
            logger.error(
                {
                    "msg": "Cannot get video statistics error content",
                    "error": json.loads(r.content),
                    "status_code": r.status_code,
                    "text": r.text,
                }
            )
        except Exception as e:
            logger.error(
                {
                    "msg": "Cannot get video statistics error content",
                    "error": e,
                    "status_code": r.status_code,
                    "text": r.text,
                }
            )
        finally:
            q.task_done()
        return

    data = json.loads(r.content)
    item = data["items"][0]["statistics"]
    views_count = int(item.get("viewCount", 0))
    likes_count = int(item.get("likeCount", 0))
    dislikes_count = int(item.get("dislikeCount", 0))

    views_str = convert_statistics_to_str(views_count)
    likes_str = convert_statistics_to_str(likes_count)
    dislikes_str = convert_statistics_to_str(dislikes_count)

    # Views per day
    try:
        views_per_day = views_count / yt_video.day
    except ZeroDivisionError:
        views_per_day = 0
    views_per_day = int(round(views_per_day, 0))
    vpd_str = convert_statistics_to_str(views_per_day)

    # Likes per day
    try:
        likes_per_day = likes_count / yt_video.day
    except ZeroDivisionError:
        likes_per_day = 0
    likes_per_day = int(round(likes_per_day, 0))

    # Rank
    rank = views_per_day + likes_per_day
    if booster_rank:
        rank *= BOOSTER_RANK_MULTIPLIER

    rank_str = convert_statistics_to_str(rank)

    result.append(
        {
            "url": yt_video.watch_url,
            "video_id": yt_video.video_id,
            "title": yt_video.title,
            "published_at": yt_video.published_at,
            "day": yt_video.day,
            "rank": rank,
            "rank_str": rank_str,
            "statistics": {
                "views": views_str,
                "likes": likes_str,
                "dislikes": dislikes_str,
                "views_count": views_count,
                "likes_count": likes_count,
                "dislikes_count": dislikes_count,
                "vpd": views_per_day,
                "vpd_str": vpd_str,
            },
        }
    )
    q.task_done()


def convert_statistics_to_str(value: int) -> str:
    """Convert statistics value to string."""
    tmp_value = value / 1000000000
    if tmp_value > 1:
        return f"{round(tmp_value, 1)}b"

    tmp_value = value / 1000000
    if tmp_value > 1:
        return f"{round(tmp_value, 1)}m"

    tmp_value = value / 1000
    if tmp_value > 1:
        return f"{round(tmp_value, 1)}k"

    return str(value)


def get_date_from_google_dt_string(data_string: str) -> dt_date:
    """Get date from TouTube datetime string."""
    # was changed recently
    google_dt_format = "%Y-%m-%dT%H:%M:%SZ"
    try:
        result_date = dt.strptime(data_string, google_dt_format).date()
    except ValueError:
        # old variant
        google_dt_format = "%Y-%m-%dT%H:%M:%S.%fZ"
        try:
            result_date = dt.strptime(data_string, google_dt_format).date()
        except ValueError:
            # if nothing else working => set today date
            result_date = dt.now().date()

    return result_date


def get_days_ago_from_date(today: dt_date, past_date: dt_date) -> int:
    """Get days ago from specific date."""
    days_ago = (today - past_date).days

    return days_ago


def write_statistics_to_file(data: list):
    """Was used for debugging."""
    sorted_data = sorted(
        data,
        key=lambda x: (x["vpd"], x["statistics"]["likes_count"]),
        reverse=True,
    )
    file_name = os.path.join(settings.BASE_DIR, "..", "yt_statistics.csv")
    fieldnames = [
        "Day",
        "URL",
        "Views",
        "Likes",
        "Dislikes",
        "Views per day",
        "L/V",
        "L/D",
        "D/V",
        "Title",
    ]
    # output_fields_dict = {el: 0 for el in fieldnames}
    with open(file_name, "w", newline="") as csvfile:
        writer = csv.DictWriter(
            csvfile,
            delimiter=",",
            quoting=csv.QUOTE_MINIMAL,
            fieldnames=fieldnames,
        )

        writer.writeheader()
        for el in sorted_data:
            try:
                lv = round(
                    el["statistics"]["likes_count"] / el["statistics"]["views_count"],
                    3,
                )
            except Exception:
                lv = 0

            try:
                ld = round(
                    el["statistics"]["likes_count"] / el["statistics"]["dislikes_count"],
                    3,
                )
            except Exception:
                ld = 0

            try:
                dv = round(
                    el["statistics"]["dislikes_count"] / el["statistics"]["views_count"],
                    3,
                )
            except Exception:
                dv = 0

            writer.writerow(
                {
                    "Day": el["day"],
                    "URL": el["url"],
                    "Views": el["statistics"]["views_count"],
                    "Likes": el["statistics"]["likes_count"],
                    "Dislikes": el["statistics"]["dislikes_count"],
                    "Views per day": el["vpd"],
                    "L/V": lv,
                    "L/D": ld,
                    "D/V": dv,
                    "Title": el["title"],
                }
            )
