import functools
import operator
from datetime import datetime as dt
from threading import Thread
from time import sleep
from typing import List

import emoji
from django.db.models import Count, Q
from django.utils import timezone as tz
from telegram import Bot, InlineKeyboardButton, InlineKeyboardMarkup
from telegram_bots.apps.top_charts.models import (
    TelegramCallbackMessage,
    TelegramCallbackUserReaction,
    TopChartTelegramChats,
)
from telegram_bots.apps.top_charts.utils.statistics import convert_statistics_to_str
from telegram_bots.constants import BOT_TOP_CHARTS_TOKEN, WEEKLY_TOP_CHART_LIMIT

__all__ = [
    "TopChartBot",
    "bot",
    "reply_markup",
    "send_bulk_videos_messages",
    "send_weekly_chart_to_telegram_chats",
]


bot = Bot(BOT_TOP_CHARTS_TOKEN)
thumb_up_btn = InlineKeyboardButton(text="\U0001F44D", callback_data="+1")
thumb_down_btn = InlineKeyboardButton(text="\U0001F44E", callback_data="-1")
reply_markup = InlineKeyboardMarkup([[thumb_up_btn, thumb_down_btn]])


# https://api.telegram.org/bot<token>/setWebhook?url=<url>/webhook/charts/top_chart_bot_007/
class TopChartBot:
    """Top chart bot class."""

    __slots__ = (
        "chat_db",
        "text",
        "reply_markup",
        "update_data",
        "message_id",
        "msg_date_timestamp",
        "callback_query_id",
        "from_id",
    )

    def __init__(self, message: dict):
        """Set init chat fields."""
        self.chat_db = TopChartTelegramChats.objects.filter(
            chat_id=str(message["message"]["chat"]["id"])
        ).first()
        self.text = message["message"]["text"]
        self.message_id = message["message"]["message_id"]
        self.msg_date_timestamp = message["message"]["date"]
        self.callback_query_id = str(message["id"])
        self.reply_markup = message["message"]["reply_markup"]["inline_keyboard"]
        self.update_data = int(message["data"])
        self.from_id = str(message["from"]["id"])

    def get_emoji_split(self, text: str) -> list:
        """Split emoji."""
        emoji_split = emoji.emoji_list(text)
        whitespace_split = [el["emoji"].split() for el in emoji_split]
        em_split = functools.reduce(operator.concat, whitespace_split)

        return em_split

    def update_counter(self):
        """Update counter for the vote."""
        if not self.reply_markup or not self.chat_db:
            return

        date_created = dt.fromtimestamp(self.msg_date_timestamp).date()
        cbck_msg, _created = TelegramCallbackMessage.objects.get_or_create(
            t_msg_id=self.message_id, defaults={"date_created": date_created}
        )

        # get or create user that voted
        user, created = TelegramCallbackUserReaction.objects.get_or_create(
            message_id=cbck_msg.pk,
            user_id=self.from_id,
            defaults={"value": self.update_data},
        )

        prev_value = user.value if not created else 0

        # update user vote
        if not created and user.value == self.update_data:
            user.value = 0
            user.save(update_fields=["value"])
        elif not created:
            user.value = self.update_data
            user.save(update_fields=["value"])

        score = (
            TelegramCallbackUserReaction.objects.filter(message_id=cbck_msg.pk)
            .annotate(plus_1_count=Count("value", filter=Q(value=1)))
            .annotate(minus_1_count=Count("value", filter=Q(value=-1)))
        )
        plus1_str = convert_statistics_to_str(score[0].plus_1_count)
        minus_1_str = convert_statistics_to_str(score[0].minus_1_count)

        result_keyboard = []
        for list_of_btns in self.reply_markup:
            for btn in list_of_btns:
                em_split = self.get_emoji_split(btn["text"])
                btn_data = int(btn["callback_data"])

                # always will be at least 2 elements in the list
                if len(em_split) == 1:
                    em_split.append("")

                # substitute last value
                if btn_data == -1:
                    if minus_1_str == "0":
                        del em_split[-1]
                    else:
                        em_split[-1] = minus_1_str
                elif btn_data == 1:
                    if plus1_str == "0":
                        del em_split[-1]
                    else:
                        em_split[-1] = plus1_str

                btn["text"] = " ".join(em_split)
                result_keyboard.append(InlineKeyboardButton(**btn))

                # send notification
                if prev_value == 0:
                    reply_msg = "You have voted"
                elif prev_value == self.update_data:
                    reply_msg = "You have cancelled your vote"
                else:
                    reply_msg = "You have changed your vote to"

                if btn_data == self.update_data:
                    bot.answer_callback_query(
                        self.callback_query_id,
                        text=f"{reply_msg} {em_split[0]}",
                    )

        # update keyboard
        _reply_markup = InlineKeyboardMarkup([result_keyboard])
        bot.edit_message_reply_markup(
            chat_id=self.chat_db.chat_id,
            message_id=self.message_id,
            reply_markup=_reply_markup,
        )


def send_bulk_videos_messages(chat_id: str, videos: List[str], today: str):
    """Send messages (str) in a suitable for chats format.

    Args:
        chat_id (str): ChatId, in which we send messages
        videos (List[str]): list of strings, info about each video
                            in suitable for the chats format
        today (str): string date representation

    Returns:
        None
    """
    for index, value in enumerate(videos):
        if index == 0:
            msg_text = f"*Weekly Top Chart ({today})*\n#topcharts\n{value}\n"
        else:
            msg_text = value

        bot.send_message(
            chat_id,
            msg_text,
            disable_notification=True,
            parse_mode="Markdown",
            reply_markup=reply_markup,
        )
        sleep(1)


def send_weekly_chart_to_telegram_chats(result_list: List[dict]):
    """Send weekly chart to telegram chats."""
    new_videos = []
    for counter, el in enumerate(result_list):
        if counter == WEEKLY_TOP_CHART_LIMIT:
            break

        new_videos.append(
            f"*{counter + 1}*. {el['title']} [{el['url']}]({el['url']}) "
            f"(\U0001F4C8 {el['rank_str']}, "
            f"\U0001F440 {el['statistics']['views']}, "
            f"\U0001F44D {el['statistics']['likes']}, "
            f"\U0001F44E {el['statistics']['dislikes']})"
        )

    today = tz.now().date().strftime("%d %b %Y")
    chats = TopChartTelegramChats.objects.filter(is_active=True).all()
    for chat in chats:
        worker = Thread(
            target=send_bulk_videos_messages,
            args=(chat.chat_id, new_videos, today),
            daemon=False,
        )
        worker.start()
