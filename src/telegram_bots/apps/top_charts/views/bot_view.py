import json
import logging

from django.http import JsonResponse
from django.views import View
from telegram_bots.apps.top_charts.models import TopChartTelegramChats

from telegram_bots.apps.top_charts.utils import TopChartBot

__all__ = ["TopChartBotView"]


logger = logging.getLogger("top_chart_bot")


class TopChartBotView(View):
    """Top Chart Bot view."""

    def post(self, request, *args, **kwargs) -> JsonResponse:
        """Process the chart bot request."""
        data = json.loads(request.body)

        if self.is_start_channel_msg(data):
            return JsonResponse({"ok": "POST request processed"})

        if not self.is_chat_can_be_processed(data):
            return JsonResponse({"ok": "POST request processed"})

        t_message = data.get("callback_query")
        if t_message:
            top_chart_bot = TopChartBot(t_message)
            top_chart_bot.update_counter()

        return JsonResponse({"ok": "POST request processed"})

    @staticmethod
    def is_start_channel_msg(data: dict) -> bool:
        """Check if it is a first time the channel messaged."""
        t_message = data.get("channel_post")
        if not t_message:
            return False

        t_chat = t_message["chat"]
        try:
            TopChartTelegramChats.objects.get_or_create(chat_id=str(t_chat["id"]))
        except Exception as e:
            logger.error(f"Exception during the chat creation: {e}")
            return False

        return True

    @staticmethod
    def is_chat_can_be_processed(data: dict) -> bool:
        """Check if it is a first time the channel messaged."""
        t_message = data.get("callback_query", {}).get("message")
        if not t_message:
            return False

        try:
            text = t_message["text"].strip().lower()
        except KeyError:
            logger.error(f"Message doesn't contain 'text' which is required: {t_message}")
            return False

        text = text.lstrip("/")
        t_chat = t_message["chat"]

        try:
            TopChartTelegramChats.objects.get_or_create(chat_id=str(t_chat["id"]))
        except Exception as e:
            logger.error(f"Exception during the chat creation: {e}")
            return False

        return True
