import json
import logging
from queue import Queue
from threading import Thread

from django.core.paginator import Paginator
from django.forms.models import model_to_dict
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils import timezone as tz
from django.views import View
from django.views.generic.base import TemplateView
from telegram_bots.apps.top_charts.mixins import YouTubeMusicChartMixin, YouTubeTrailerChartMixin
from telegram_bots.apps.top_charts.models import YouTubeChart
from telegram_bots.apps.top_charts.tasks import (
    async_fetch_top_all_time_videos,
    async_fetch_top_monthly_videos,
    async_fetch_top_ru_ua_monthly_videos,
    async_fetch_top_ru_ua_weekly_videos,
    async_fetch_top_ww_weekly_videos,
    async_periodic_fetch_chart_today,
)
from telegram_bots.apps.top_charts.utils import (
    clean_up_charts,
    days_ago_iso_end,
    days_ago_iso_start,
    get_chart,
    get_chart_request_parameters,
    is_allowed_run_chart_task,
    is_allowed_run_weekly_chart_task,
    last_month_video_chart,
)
from telegram_bots.constants import (
    ALL_TIME_MUSIC_REQUEST_DATA,
    MUSIC_VIDEO_CATEGORY_ID,
    TRAILERS_REQUEST_DATA,
    VIDEOS_PER_PAGE,
    VIDEOS_TO_SHOW,
    WEEKLY_RU_REQUEST_DATA,
    WEEKLY_WORLDWIDE_REQUEST_DATA,
    WW_MUSIC_REQUEST_DATA,
)

__all__ = [
    "WWMusicVideoChartView",
    "RUMusicVideoChartView",
    "TrailersChartView",
    "ATMusicVideoChartView",
    "PeriodicMonthlyChartTaskView",
    "PeriodicWeeklyChartTaskView",
    "ww_music_video_chart_view",
    "ww_ua_and_ru_video_chart_view",
    "trailers_chart_view",
    "all_time_music_video_chart_view",
]

logger = logging.getLogger(__name__)


class WWMusicVideoChartView(YouTubeMusicChartMixin, TemplateView):
    """Monthly chart, US region."""

    template_name = "top_chart.html"
    chart_name = "New Worldwide Music Videos"
    help_text = (
        f"My {chart_name} top chart is generated once per "
        "month for videos that were published during the last month. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube. "
        f"Algorithm for generating {chart_name} top chart "
        "is based on views, published date, likes and dislikes."
    )

    def get_context_data(self, **kwargs) -> dict:
        """Get context data."""
        ctx = super().get_context_data(**kwargs)
        ctx["top_title"] = f"{self.chart_name} (monthly)"
        ctx["meta_description"] = (
            f"{ctx.get('meta_description', '')}"
            f"Top chart of {self.chart_name} published not later than "
            f"one month ago."
        )
        ctx["help_text"] = self.help_text
        request_data = {**self.base_request_data, **WW_MUSIC_REQUEST_DATA}
        video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.WORLDWIDE)
        ctx["title"] = f"{self.chart_name} top chart ({video_chart.name})"
        if not _created:
            if not video_chart.processed:
                return ctx

            _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
            paginator = Paginator(_query, VIDEOS_PER_PAGE)
            page = self.request.GET.get("page")
            videos_list = paginator.get_page(page)
            ctx["items"] = videos_list
            ctx["chart"] = video_chart
            return ctx

        logger.info(
            "WorldwideMusicVideos not exists for the last months. Fetching monthly videos."
        )
        worker = Thread(
            target=async_fetch_top_monthly_videos,
            args=(request_data, video_chart.pk),
            daemon=False,
        )
        worker.start()

        cleanup_worker = Thread(
            target=clean_up_charts,
            args=(video_chart.chart_type,),
            daemon=False,
        )
        cleanup_worker.start()

        return ctx


def ww_music_video_chart_view(request: HttpRequest) -> JsonResponse:
    """Monthly chart, US region."""
    chart_name = "New Worldwide Music Videos"
    help_text = (
        f"My {chart_name} top chart is generated once per "
        "month for videos that were published during the last month. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube. "
        f"Algorithm for generating {chart_name} top chart "
        "is based on views, published date, likes and dislikes."
    )

    mixin = YouTubeMusicChartMixin()
    ctx = mixin.get_context_data()
    ctx["top_title"] = f"{chart_name} (monthly)"
    ctx["meta_description"] = (
        f"{ctx.get('meta_description', '')}"
        f"Top chart of {chart_name} published not later than "
        f"one month ago."
    )
    ctx["help_text"] = help_text
    request_data = {**YouTubeMusicChartMixin.base_request_data, **WW_MUSIC_REQUEST_DATA}
    video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.WORLDWIDE)
    ctx["title"] = f"{chart_name} top chart ({video_chart.name})"
    if not _created:
        if not video_chart.processed:
            return JsonResponse(ctx)

        _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
        paginator = Paginator(_query, VIDEOS_PER_PAGE)
        page = request.GET.get("page")
        videos_list = paginator.get_page(page)
        ctx["items"] = [model_to_dict(el) for el in videos_list]
        ctx["chart"] = model_to_dict(video_chart)
        ctx["pages"] = {
            "num_pages": videos_list.paginator.num_pages,
            "current_page": videos_list.number,
        }
        response = JsonResponse(ctx)
        return response

    logger.info("WorldwideMusicVideos not exists for the last months. Fetching monthly videos.")
    worker = Thread(
        target=async_fetch_top_monthly_videos,
        args=(request_data, video_chart.pk),
        daemon=False,
    )
    worker.start()

    cleanup_worker = Thread(
        target=clean_up_charts,
        args=(video_chart.chart_type,),
        daemon=False,
    )
    cleanup_worker.start()

    return JsonResponse(ctx)


class RUMusicVideoChartView(YouTubeMusicChartMixin, TemplateView):
    """Monthly chart, RU/UA region."""

    template_name = "top_chart.html"
    chart_name = "New Russian Music Videos"
    help_text = (
        f"My {chart_name} top chart is generated once per month "
        "for videos that were published during the last month. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube. "
        f"Algorithm for generating {chart_name} top chart is "
        "based on views, published date, likes and dislikes."
    )

    def get_context_data(self, **kwargs) -> dict:
        """Get context data."""
        ctx = super().get_context_data(**kwargs)
        ctx["top_title"] = f"{self.chart_name} (monthly)"
        ctx["meta_description"] = (
            f"{ctx.get('meta_description', '')}"
            f"Top chart of {self.chart_name} published not later "
            f"than one month ago."
        )
        ctx["help_text"] = self.help_text
        video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.RU)
        ctx["title"] = f"{self.chart_name} top chart ({video_chart.name})"
        if not _created:
            if not video_chart.processed:
                return ctx

            _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
            paginator = Paginator(_query, VIDEOS_PER_PAGE)
            page = self.request.GET.get("page")
            videos_list = paginator.get_page(page)
            ctx["items"] = videos_list
            ctx["chart"] = video_chart
            return ctx

        worker = Thread(
            target=async_fetch_top_ru_ua_monthly_videos,
            args=(video_chart.pk,),
            daemon=False,
        )
        worker.start()

        cleanup_worker = Thread(
            target=clean_up_charts,
            args=(video_chart.chart_type,),
            daemon=False,
        )
        cleanup_worker.start()

        return ctx


def ww_ua_and_ru_video_chart_view(request: HttpRequest) -> JsonResponse:
    """Monthly chart, RU/UA region."""
    chart_name = "New UA and RU Music Videos"
    help_text = (
        f"My {chart_name} top chart is generated once per "
        "month for videos that were published during the last month. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube. "
        f"Algorithm for generating {chart_name} top chart "
        "is based on views, published date, likes and dislikes."
    )

    mixin = YouTubeMusicChartMixin()
    ctx = mixin.get_context_data()
    ctx["top_title"] = f"{chart_name} (monthly)"
    ctx["meta_description"] = (
        f"{ctx.get('meta_description', '')}"
        f"Top chart of {chart_name} published not later than "
        f"one month ago."
    )
    ctx["help_text"] = help_text
    video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.RU)
    ctx["title"] = f"{chart_name} top chart ({video_chart.name})"
    if not _created:
        if not video_chart.processed:
            return JsonResponse(ctx)

        _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
        paginator = Paginator(_query, VIDEOS_PER_PAGE)
        page = request.GET.get("page")
        videos_list = paginator.get_page(page)
        ctx["items"] = [model_to_dict(el) for el in videos_list]
        ctx["chart"] = model_to_dict(video_chart)
        ctx["pages"] = {
            "num_pages": videos_list.paginator.num_pages,
            "current_page": videos_list.number,
        }
        response = JsonResponse(ctx)
        return response

    logger.info(
        "UA and RU music top chart not exists for the last months. Fetching monthly videos."
    )
    worker = Thread(
        target=async_fetch_top_ru_ua_monthly_videos,
        args=(video_chart.pk,),
        daemon=False,
    )
    worker.start()

    cleanup_worker = Thread(
        target=clean_up_charts,
        args=(video_chart.chart_type,),
        daemon=False,
    )
    cleanup_worker.start()

    return JsonResponse(ctx)


class TrailersChartView(YouTubeTrailerChartMixin, TemplateView):
    """Monthly chart, trailers."""

    template_name = "top_chart.html"
    chart_name = "New Movie Trailers"
    help_text = (
        f"My {chart_name} top chart is generated once per month "
        "for videos that were published during the last month. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube. "
        f"Algorithm for generating {chart_name} top chart is "
        "based on views, published date, likes and dislikes."
    )

    def get_context_data(self, **kwargs) -> dict:
        """Get context data."""
        ctx = super().get_context_data(**kwargs)
        ctx["top_title"] = f"{self.chart_name} (monthly)"
        ctx["meta_description"] = (
            f"{ctx.get('meta_description', '')}"
            f"Top chart of {self.chart_name} published not later "
            "than one month ago."
        )
        ctx["help_text"] = self.help_text
        request_data = {**self.base_request_data, **TRAILERS_REQUEST_DATA}
        video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.TRAILER)
        ctx["title"] = f"{self.chart_name} top chart ({video_chart.name})"
        if not _created:
            if not video_chart.processed:
                return ctx

            _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
            paginator = Paginator(_query, VIDEOS_PER_PAGE)
            page = self.request.GET.get("page")
            videos_list = paginator.get_page(page)
            ctx["items"] = videos_list
            ctx["chart"] = video_chart
            return ctx

        worker = Thread(
            target=async_fetch_top_monthly_videos,
            args=(request_data, video_chart.pk),
            daemon=False,
        )
        worker.start()

        cleanup_worker = Thread(
            target=clean_up_charts,
            args=(video_chart.chart_type,),
            daemon=False,
        )
        cleanup_worker.start()

        return ctx


def trailers_chart_view(request: HttpRequest) -> JsonResponse:
    """Monthly chart, trailers."""
    chart_name = "New Movie Trailers"
    help_text = (
        f"My {chart_name} top chart is generated once per month "
        "for videos that were published during the last month. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube. "
        f"Algorithm for generating {chart_name} top chart is "
        "based on views, published date, likes and dislikes."
    )

    mixin = YouTubeMusicChartMixin()
    ctx = mixin.get_context_data()
    ctx["top_title"] = f"{chart_name} (monthly)"
    ctx["meta_description"] = (
        f"{ctx.get('meta_description', '')}"
        f"Top chart of {chart_name} published not later "
        "than one month ago."
    )
    ctx["help_text"] = help_text
    request_data = {**YouTubeMusicChartMixin.base_request_data, **TRAILERS_REQUEST_DATA}
    video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.TRAILER)
    ctx["title"] = f"{chart_name} top chart ({video_chart.name})"
    if not _created:
        if not video_chart.processed:
            return ctx

        _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
        paginator = Paginator(_query, VIDEOS_PER_PAGE)
        page = request.GET.get("page")
        videos_list = paginator.get_page(page)
        ctx["items"] = [model_to_dict(el) for el in videos_list]
        ctx["chart"] = model_to_dict(video_chart)
        ctx["pages"] = {
            "num_pages": videos_list.paginator.num_pages,
            "current_page": videos_list.number,
        }
        response = JsonResponse(ctx)
        return response

    worker = Thread(
        target=async_fetch_top_monthly_videos,
        args=(request_data, video_chart.pk),
        daemon=False,
    )
    worker.start()

    cleanup_worker = Thread(
        target=clean_up_charts,
        args=(video_chart.chart_type,),
        daemon=False,
    )
    cleanup_worker.start()

    return JsonResponse(ctx)


class ATMusicVideoChartView(YouTubeMusicChartMixin, TemplateView):
    """All time chart, update each month."""

    template_name = "top_chart.html"
    chart_name = "All Time Music Videos"
    help_text = (
        f"{chart_name} top chart is generated once per month "
        "for all music videos that are on YouTube. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube."
    )

    def get_context_data(self, **kwargs) -> dict:
        """Get context data."""
        ctx = super().get_context_data(**kwargs)
        ctx["top_title"] = f"{self.chart_name}"
        ctx[
            "meta_description"
        ] = f"{ctx.get('meta_description', '')} Top chart of {self.chart_name}."
        ctx["help_text"] = self.help_text
        request_data = {
            **self.base_request_data,
            **ALL_TIME_MUSIC_REQUEST_DATA,
        }
        video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.ALL_TIME)
        ctx["title"] = f"{self.chart_name} top chart ({video_chart.name})"
        if not _created:
            if not video_chart.processed:
                return ctx

            _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
            paginator = Paginator(_query, VIDEOS_PER_PAGE)
            page = self.request.GET.get("page")
            videos_list = paginator.get_page(page)
            ctx["items"] = videos_list
            ctx["chart"] = video_chart
            return ctx

        worker = Thread(
            target=async_fetch_top_all_time_videos,
            args=(request_data, video_chart.pk),
            daemon=False,
        )
        worker.start()

        cleanup_worker = Thread(
            target=clean_up_charts,
            args=(video_chart.chart_type,),
            daemon=False,
        )
        cleanup_worker.start()

        return ctx


def all_time_music_video_chart_view(request: HttpRequest) -> JsonResponse:
    """All time chart, update each month."""
    chart_name = "All Time Music Videos"
    help_text = (
        f"{chart_name} top chart is generated once per month "
        "for all music videos that are on YouTube. "
        "Videos statistics are shown on the moment the top chart was generated. "
        "Sometimes videos are not allowed to be played on the web page. "
        "In such case please follow the link to view the video on YouTube."
    )

    mixin = YouTubeMusicChartMixin()
    ctx = mixin.get_context_data()
    ctx["top_title"] = f"{chart_name}"
    ctx["meta_description"] = f"{ctx.get('meta_description', '')} Top chart of {chart_name}."
    ctx["help_text"] = help_text
    request_data = {
        **YouTubeMusicChartMixin.base_request_data,
        **ALL_TIME_MUSIC_REQUEST_DATA,
    }
    video_chart, _created = last_month_video_chart(chart_type=YouTubeChart.ALL_TIME)
    ctx["title"] = f"{chart_name} top chart ({video_chart.name})"
    if not _created:
        if not video_chart.processed:
            return ctx

        _query = video_chart.videos.all().order_by("position")[:VIDEOS_TO_SHOW]
        paginator = Paginator(_query, VIDEOS_PER_PAGE)
        page = request.GET.get("page")
        videos_list = paginator.get_page(page)
        ctx["items"] = [model_to_dict(el) for el in videos_list]
        ctx["chart"] = model_to_dict(video_chart)
        ctx["pages"] = {
            "num_pages": videos_list.paginator.num_pages,
            "current_page": videos_list.number,
        }
        response = JsonResponse(ctx)
        return response

    worker = Thread(
        target=async_fetch_top_all_time_videos,
        args=(request_data, video_chart.pk),
        daemon=False,
    )
    worker.start()

    cleanup_worker = Thread(
        target=clean_up_charts,
        args=(video_chart.chart_type,),
        daemon=False,
    )
    cleanup_worker.start()

    return JsonResponse(ctx)


class PeriodicMonthlyChartTaskView(View):
    """Periodic Monthly Chart task view."""

    def post(self, request, *args, **kwargs) -> JsonResponse:
        """Process the request."""
        try:
            data = json.loads(request.body)
        except Exception as e:
            msg = "Only JSON requests are allowed"
            logger.error({"msg": msg, "error": e})
            return JsonResponse({"error": msg}, status=406)

        if not is_allowed_run_chart_task(data):
            msg = "You are not allowed to make this request"
            logger.error({"msg": msg})
            return JsonResponse({"error": msg}, status=401)

        today = tz.now().date()
        chart = YouTubeChart.objects.filter(date=today).first()
        if chart:
            msg = (
                "Today some chart was already run. "
                f"Name: {chart.name}, type: {chart.chart_type}"
            )
            logger.error({"msg": msg})
            return JsonResponse({"error": msg}, status=406)

        chart_type = str(data.get("chart_type", "")).upper()
        request_data = get_chart_request_parameters(chart_type)
        if not request_data:
            msg = f"Incorrect chart type: {chart_type}. Not able to set request parameters,"
            logger.error({"msg": msg})
            return JsonResponse({"error": msg}, status=406)

        worker = Thread(
            target=async_periodic_fetch_chart_today,
            args=(chart_type, request_data),
            daemon=False,
        )
        worker.start()

        return JsonResponse({"ok": "Tasks started"})


class PeriodicWeeklyChartTaskView(View):
    """Periodic Weekly Chart Task View."""

    def post(self, request, *args, **kwargs) -> JsonResponse:
        """Process the request."""
        try:
            data = json.loads(request.body)
        except Exception as e:
            msg = "Only JSON requests are allowed"
            logger.error({"msg": msg, "error": e})
            return JsonResponse({"error": msg}, status=406)

        if not is_allowed_run_weekly_chart_task(data):
            msg = "You are not allowed to make this request"
            logger.error({"msg": msg})
            return JsonResponse({"error": msg}, status=401)

        chart_region = str(data.get("chart_region", "ww"))
        if chart_region.lower() == "ru":
            request_params = {**WEEKLY_RU_REQUEST_DATA}
            worker = Thread(
                target=async_fetch_top_ru_ua_weekly_videos,
                args=(request_params,),
                daemon=False,
            )
        else:
            request_params = {**WEEKLY_WORLDWIDE_REQUEST_DATA}
            worker = Thread(
                target=async_fetch_top_ww_weekly_videos,
                args=(request_params,),
                daemon=False,
            )

        worker.start()

        return JsonResponse({"ok": "Tasks started"})


class DailyMusicVideoChartView(View):
    """Daily Music Video Chart View."""

    template_name = "top_chart.html"
    params = {
        "q": "official video",
        "order": "viewCount",
        "part": "snippet",
        "maxResults": 10,
        "regionCode": "US",
        "videoCategoryId": MUSIC_VIDEO_CATEGORY_ID,
        "type": "video",
        "topicId": "/m/04rlf",
    }

    def get(self, request, *args, **kwargs) -> HttpResponse:
        """Get chart from the DB."""
        q: Queue = Queue()
        result_list: list = []
        days_ago = request.GET.get("days", "30")
        days_ago = int(days_ago)
        q.put(days_ago)
        self.params["publishedAfter"] = days_ago_iso_start(days_ago)
        self.params["publishedBefore"] = days_ago_iso_end(days_ago)
        get_chart(self.params, result_list, q)
        return render(request, self.template_name, {"items": result_list})
