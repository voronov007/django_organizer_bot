import typing

from django.views.generic.base import TemplateView
from telegram_bots.constants import MUSIC_VIDEO_CATEGORY_ID, TRAILER_VIDEO_CATEGORY_ID

if typing.TYPE_CHECKING:
    _Base: TemplateView
else:
    _Base = object


class YouTubeMusicChartMixin(_Base):
    """YouTube Music Mixin class."""

    base_request_data = {
        "order": "viewCount",
        "part": "snippet",
        "maxResults": 2,
        "videoCategoryId": MUSIC_VIDEO_CATEGORY_ID,
        "type": "video",
        "topicId": "/m/04rlf",
    }

    def get_context_data(self, **kwargs) -> dict:
        """Get context data."""
        try:
            ctx = super().get_context_data(**kwargs)
        except Exception:
            ctx = {}

        ctx["items"] = []
        ctx["chart"] = {"name": "Processing videos. Please come back in a minute."}
        ctx[
            "meta_description"
        ] = "Top chart of YouTube videos. Enjoy top videos which are InMyTop. "
        ctx["meta_keywords"] = "YouTube, videos, top chart, music"
        return ctx


class YouTubeTrailerChartMixin(_Base):
    """YouTube Trailer Chart Mixin class."""

    base_request_data = {
        "order": "viewCount",
        "part": "snippet",
        "maxResults": 2,
        "videoCategoryId": TRAILER_VIDEO_CATEGORY_ID,
        "type": "video",
    }

    def get_context_data(self, **kwargs) -> dict:
        """Get context data."""
        ctx = super().get_context_data(**kwargs)
        ctx["items"] = []
        ctx["chart"] = {"name": "Processing videos. Please come back in a minute."}
        ctx[
            "meta_description"
        ] = "Top chart of YouTube trailer videos. Enjoy top videos which are InMyTop. "
        ctx["meta_keywords"] = "YouTube, videos, top chart, trailer, movie, animation"
        return ctx
