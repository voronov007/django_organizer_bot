from django.apps import AppConfig


class TopChartsConfig(AppConfig):
    """Top Charts config class."""

    name = "telegram_bots.apps.top_charts"
