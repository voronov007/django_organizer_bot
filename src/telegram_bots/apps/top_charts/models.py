import datetime as dt

from django.db import models


class YouTubeChart(models.Model):
    """YouTube Chart model."""

    name = models.CharField(max_length=50, blank=False)
    date = models.DateField(db_index=True)
    processed = models.BooleanField(default=False)

    WORLDWIDE = "WW"
    RU = "RU"
    ALL_TIME = "AT"
    TRAILER = "TR"
    CHART_TYPE_CHOICES = [
        (WORLDWIDE, "Worldwide"),
        (RU, "RU"),
        (ALL_TIME, "All time"),
        (TRAILER, "Trailers"),
    ]
    chart_type = models.CharField(
        max_length=2,
        choices=CHART_TYPE_CHOICES,
        default=WORLDWIDE,
        db_index=True,
    )

    class Meta:
        """Meta class."""

        verbose_name = "YouTube Chart"
        verbose_name_plural = "YouTube Charts"
        unique_together = (("date", "chart_type"),)


class YouTubeVideo(models.Model):
    """YouTube Vido model."""

    chart = models.ForeignKey(YouTubeChart, on_delete=models.CASCADE, related_name="videos")

    published_at = models.DateField(default=dt.datetime.today, blank=False, null=False)
    position = models.PositiveIntegerField()
    video_id = models.CharField(max_length=50, blank=False, null=False)
    title = models.CharField(max_length=150, blank=False, null=False)
    url = models.URLField(blank=False, null=False)
    vpd = models.BigIntegerField(blank=False, null=False)
    vpd_str = models.CharField(max_length=10, default="1k", blank=False, null=False)
    rank = models.BigIntegerField(blank=False, null=False, default=1)
    rank_str = models.CharField(max_length=10, default="1k", blank=False, null=False)
    views_str = models.CharField(max_length=50, blank=False, null=False)
    likes_str = models.CharField(max_length=50, blank=False, null=False)
    dislikes_str = models.CharField(max_length=50, blank=False, null=False)
    views_num = models.BigIntegerField(blank=False, null=False)
    likes_num = models.BigIntegerField(blank=False, null=False)
    dislikes_num = models.BigIntegerField(blank=False, null=False)

    class Meta:
        """Meta class."""

        verbose_name = "YouTube Video"
        verbose_name_plural = "YouTube Videos"


class TopChartTelegramChats(models.Model):
    """Telegram chat model for Top Charts."""

    chat_id = models.CharField(max_length=150, blank=False, null=False, unique=True, db_index=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        """Meta class."""

        verbose_name = "TopChart Telegram Chat"
        verbose_name_plural = "TopChart Telegram Chats"


class TelegramCallbackMessage(models.Model):
    """Callback message class."""

    t_msg_id = models.CharField(max_length=150, unique=True, db_index=True)
    date_created = models.DateField()

    class Meta:
        """Meta class."""

        verbose_name = "Telegram Callback message"
        verbose_name_plural = "Telegram Callback messages"


class TelegramCallbackUserReaction(models.Model):
    """Callback user reaction class."""

    message = models.ForeignKey(
        TelegramCallbackMessage, on_delete=models.CASCADE, related_name="users"
    )

    # 0, 1, -1
    value = models.SmallIntegerField()
    user_id = models.CharField(max_length=150, db_index=True)

    class Meta:
        """Meta class."""

        verbose_name = "Telegram Callback user reaction"
        verbose_name_plural = "Telegram Callback user reactions"
        unique_together = ["message", "user_id"]
