import logging
from queue import Queue
from threading import Thread
from typing import List

from telegram_bots.apps.top_charts.models import YouTubeChart, YouTubeVideo
from telegram_bots.apps.top_charts.utils import (
    days_ago_iso_end,
    days_ago_iso_start,
    filter_result_by_threshold,
    get_all_time_chart,
    get_chart,
    get_dates_request_params,
    get_or_create_today_video_chart,
    get_youtube_iso_history_date,
    send_weekly_chart_to_telegram_chats,
    sort_list_by_rank,
)
from telegram_bots.constants import (
    RU_MUSIC_REQUEST_DATA,
    UA_MUSIC_REQUEST_DATA,
    VIDEO_MONTHLY_CHART_END_RANGE,
    VIDEO_MONTHLY_CHART_START_RANGE,
    VIDEO_WEEKLY_CHART_END_RANGE,
    VIDEO_WEEKLY_CHART_START_RANGE,
    VIEWS_THRESHOLD,
    WEEKLY_UA_REQUEST_DATA,
)

logger = logging.getLogger("top_chart_async_tasks")


def async_fetch_top_monthly_videos(r_params: dict, video_chart_id: int):
    """Fetch top monthly videos."""
    result_list: list = []
    q: Queue = Queue()

    for days_ago in range(VIDEO_MONTHLY_CHART_START_RANGE, VIDEO_MONTHLY_CHART_END_RANGE):
        q.put(days_ago)

        end_date = days_ago_iso_end(days_ago)
        start_date = days_ago_iso_start(days_ago)
        updated_r_params = get_dates_request_params(start_date=start_date, end_date=end_date)
        updated_r_params.update(r_params)

        worker = Thread(
            target=get_chart,
            args=(updated_r_params, result_list, q),
            daemon=False,
        )
        worker.start()

    q.join()

    logger.info(f"Number of videos in the chart before filtering by threshold: {len(result_list)}")
    result_list = filter_result_by_threshold(result=result_list, threshold=VIEWS_THRESHOLD)
    logger.info(f"Number of videos in the chart after filtering by threshold: {len(result_list)}")
    sorted_data = sort_list_by_rank(result_list)

    new_videos = []
    for counter, el in enumerate(sorted_data):
        new_videos.append(
            YouTubeVideo(
                chart_id=video_chart_id,
                position=counter + 1,
                video_id=el["video_id"],
                title=el["title"],
                published_at=el["published_at"],
                url=el["url"],
                rank=el["rank"],
                rank_str=el["rank_str"],
                vpd=el["statistics"]["vpd"],
                vpd_str=el["statistics"]["vpd_str"],
                views_str=el["statistics"]["views"],
                likes_str=el["statistics"]["likes"],
                dislikes_str=el["statistics"]["dislikes"],
                views_num=el["statistics"]["views_count"],
                likes_num=el["statistics"]["likes_count"],
                dislikes_num=el["statistics"]["dislikes_count"],
            )
        )

    try:
        YouTubeVideo.objects.bulk_create(new_videos)
        YouTubeChart.objects.filter(pk=video_chart_id).update(processed=True)
    except Exception as e:
        logger.error({"error": e, "msg": "Error during bulk create MusicVideos"})


def async_fetch_top_ru_ua_monthly_videos(video_chart_id: int):
    """Fetch top monthly UA & RU videos."""
    result_list: list = []
    result_ua_list: list = []
    threshold_ua = VIEWS_THRESHOLD // 4
    threshold_ru = VIEWS_THRESHOLD // 2
    q: Queue = Queue()

    end_date = days_ago_iso_end(VIDEO_MONTHLY_CHART_START_RANGE)
    start_date = days_ago_iso_start(VIDEO_MONTHLY_CHART_END_RANGE)
    dates_params = get_dates_request_params(start_date=start_date, end_date=end_date)

    run_get_chart_worker(
        q=q,
        q_value="monthly ru chart",
        result=result_list,
        request_params=RU_MUSIC_REQUEST_DATA,
        dates_params=dates_params,
    )

    run_get_chart_worker(
        q=q,
        q_value="monthly ua chart",
        result=result_ua_list,
        request_params=UA_MUSIC_REQUEST_DATA,
        dates_params=dates_params,
        booster_rank=True,
    )

    q.join()

    result_ua_list = filter_result_by_threshold(result=result_ua_list, threshold=threshold_ua)
    result_list = filter_result_by_threshold(result=result_list, threshold=threshold_ru)
    result_list.extend(result_ua_list)
    sorted_data = sort_list_by_rank(result_list)

    new_videos = []
    for counter, el in enumerate(sorted_data):
        new_videos.append(
            YouTubeVideo(
                chart_id=video_chart_id,
                position=counter + 1,
                video_id=el["video_id"],
                title=el["title"],
                published_at=el["published_at"],
                url=el["url"],
                rank=el["rank"],
                rank_str=el["rank_str"],
                vpd=el["statistics"]["vpd"],
                vpd_str=el["statistics"]["vpd_str"],
                views_str=el["statistics"]["views"],
                likes_str=el["statistics"]["likes"],
                dislikes_str=el["statistics"]["dislikes"],
                views_num=el["statistics"]["views_count"],
                likes_num=el["statistics"]["likes_count"],
                dislikes_num=el["statistics"]["dislikes_count"],
            )
        )

    try:
        YouTubeVideo.objects.bulk_create(new_videos)
        YouTubeChart.objects.filter(pk=video_chart_id).update(processed=True)
    except Exception as e:
        logger.error({"error": e, "msg": "Error during bulk create MusicVideos"})


def async_fetch_top_all_time_videos(r_params: dict, video_chart_id: int):
    """Fetch top all time videos."""
    result_list: list = []

    end_date = days_ago_iso_end(VIDEO_MONTHLY_CHART_START_RANGE)  # several months ago
    start_date = get_youtube_iso_history_date()
    dates_params = get_dates_request_params(start_date=start_date, end_date=end_date)
    r_params.update(**dates_params)

    get_all_time_chart(r_params, result_list)

    # Don't look when published so `vpd` == `views count`
    sorted_data = sorted(result_list, key=lambda x: x["statistics"]["vpd"], reverse=True)

    new_videos = []
    for counter, el in enumerate(sorted_data):
        new_videos.append(
            YouTubeVideo(
                chart_id=video_chart_id,
                position=counter + 1,
                video_id=el["video_id"],
                title=el["title"],
                published_at=el["published_at"],
                url=el["url"],
                rank=el["statistics"]["vpd"],
                rank_str=el["statistics"]["vpd_str"],
                vpd=el["statistics"]["vpd"],
                vpd_str=el["statistics"]["vpd_str"],
                views_str=el["statistics"]["views"],
                likes_str=el["statistics"]["likes"],
                dislikes_str=el["statistics"]["dislikes"],
                views_num=el["statistics"]["views_count"],
                likes_num=el["statistics"]["likes_count"],
                dislikes_num=el["statistics"]["dislikes_count"],
            )
        )

    try:
        YouTubeVideo.objects.bulk_create(new_videos)
        YouTubeChart.objects.filter(pk=video_chart_id).update(processed=True)
    except Exception as e:
        logger.error({"error": e, "msg": "Error during bulk create MusicVideos"})


def async_periodic_fetch_chart_today(chart_type: str, request_data: dict):
    """Fetch today top chart videos."""
    video_chart, created = get_or_create_today_video_chart(chart_type)
    if created:
        if video_chart.chart_type == YouTubeChart.ALL_TIME:
            worker = Thread(
                target=async_fetch_top_all_time_videos,
                args=(request_data, video_chart.pk),
                daemon=False,
            )
        elif video_chart.chart_type == YouTubeChart.RU:
            worker = Thread(
                target=async_fetch_top_ru_ua_monthly_videos,
                args=(video_chart.pk,),
                daemon=False,
            )
        else:
            worker = Thread(
                target=async_fetch_top_monthly_videos,
                args=(request_data, video_chart.pk),
                daemon=False,
            )
        worker.start()


def async_fetch_top_ww_weekly_videos(r_params: dict):
    """Fetch top worldwide weekly videos."""
    result_list: list = []
    q: Queue = Queue()

    end_date = days_ago_iso_end(VIDEO_WEEKLY_CHART_START_RANGE)
    start_date = days_ago_iso_start(VIDEO_WEEKLY_CHART_END_RANGE)
    dates_params = get_dates_request_params(start_date=start_date, end_date=end_date)

    run_get_chart_worker(
        q=q,
        q_value="weekly ww chart",
        result=result_list,
        request_params=r_params,
        dates_params=dates_params,
    )
    q.join()

    result_list = filter_result_by_threshold(result=result_list, threshold=VIEWS_THRESHOLD)
    sorted_data = sort_list_by_rank(result_list)
    send_weekly_chart_to_telegram_chats(sorted_data)


def async_fetch_top_ru_ua_weekly_videos(r_params: dict):
    """Fetch top UA & RU weekly videos."""
    result_list: list = []
    result_ua_list: list = []
    threshold_ua = VIEWS_THRESHOLD // 4
    threshold_ru = VIEWS_THRESHOLD // 2
    q: Queue = Queue()

    end_date = days_ago_iso_end(VIDEO_WEEKLY_CHART_START_RANGE)
    start_date = days_ago_iso_start(VIDEO_WEEKLY_CHART_END_RANGE)
    dates_params = get_dates_request_params(start_date=start_date, end_date=end_date)

    run_get_chart_worker(
        q=q,
        q_value="weekly ru chart",
        result=result_list,
        request_params=r_params,
        dates_params=dates_params,
    )

    run_get_chart_worker(
        q=q,
        q_value="weekly ua chart",
        result=result_ua_list,
        request_params={**WEEKLY_UA_REQUEST_DATA},
        dates_params=dates_params,
        booster_rank=True,
    )

    q.join()

    result_ua_list = filter_result_by_threshold(result=result_ua_list, threshold=threshold_ua)
    result_list = filter_result_by_threshold(result=result_list, threshold=threshold_ru)
    result_list.extend(result_ua_list)
    sorted_data = sort_list_by_rank(result_list)
    send_weekly_chart_to_telegram_chats(sorted_data)


def run_get_chart_worker(
    q: Queue,
    q_value: str,
    result: List[dict],
    request_params: dict,
    dates_params: dict,
    booster_rank: bool = False,
):
    """Fetch chart."""
    request_params_copy = {**dates_params}
    request_params_copy.update(request_params)

    q.put(q_value)
    worker = Thread(
        target=get_chart,
        args=(request_params_copy, result, q, booster_rank),
        daemon=False,
    )
    worker.start()
