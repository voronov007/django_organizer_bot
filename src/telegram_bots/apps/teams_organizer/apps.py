from django.apps import AppConfig


class TeamsOrganizerConfig(AppConfig):
    """Teams Organizer Config class."""

    name = "telegram_bots.apps.teams_organizer"
