import json
import os
import re
from pickle import dumps as pickle_dumps  # noqa: S403
from pickle import loads as pickle_loads  # noqa: S403

import requests
from django.utils import translation
from django.utils.translation import gettext as _
from telegram_bots.apps.teams_organizer.constants import (
    BOT_LINK,
    GUESTS_LIMIT,
    MAX_LIMIT,
    MIN_LIMIT,
    TOTAL_LIMIT,
)
from telegram_bots.apps.teams_organizer.models import TeamOrganizerCollection
from telegram_bots.constants import TELEGRAM_URL
from telegram_bots.utils.lang import get_default_language, language_exists

BOT_TEAM_ORGANIZER_TOKEN = os.getenv("TEAM_ORGANIZER_TOKEN", "error_token")
ADMINS_LIST = {"creator", "administrator"}

__all__ = ["TeamOrganizer"]


class Guests:
    NAME = 0
    NUMBER = 1


class Person:
    def __init__(self, person_data: dict):
        self.language_code = person_data.get("language_code", "en")
        self.id = str(person_data["id"])
        self.person_str = self.__get_person_string(person_data)

    @staticmethod
    def __get_person_string(person: dict) -> str:
        username = person.get("username")
        first_name = person.get("first_name", "")
        last_name = person.get("last_name", "")
        if not username:
            return f"{first_name} {last_name} "

        return f"@{username}({first_name} {last_name})"


class TeamOrganizer:
    """Team Organizer.

    {
        <chat_id>: {
            "persons": {},
            "guests": {
                "person_id": N
            },
            "price": 0,
            "limit": 15
        }
    }
    """

    __slots__ = ("person", "chat_id", "t_chat_dict", "chat")

    def __init__(self, t_chat: dict, person_data: dict):
        """Teams Organizer init."""
        self.person = Person(person_data=person_data)
        self.t_chat_dict = t_chat
        self.chat_id = str(t_chat["id"])
        self.chat = self.__check_chat_db(self.chat_id)

    def __check_chat_db(self, chat_id: str) -> TeamOrganizerCollection:
        chat = TeamOrganizerCollection.objects.filter(chat_id=chat_id).first()
        if not chat:
            chat = TeamOrganizerCollection.objects.create(
                chat_id=chat_id,
                price=0,
                limit=TOTAL_LIMIT,
                language_code=get_default_language(self.person.language_code),
                participants=pickle_dumps({"persons": {}, "guests": {}}),
            )

        translation.activate(chat.language_code)

        return chat

    def __is_admin(self) -> bool:
        if self.t_chat_dict["type"] in ("group", "supergroup"):
            if not self.t_chat_dict["all_members_are_administrators"]:
                data = {"chat_id": self.chat_id, "user_id": self.person.id}
                response = requests.post(
                    f"{TELEGRAM_URL}{BOT_TEAM_ORGANIZER_TOKEN}/getChatMember", data=data, timeout=5
                )
                response_dict = json.loads(response.text)
                if response_dict["ok"]:
                    if response_dict["result"]["status"] not in ADMINS_LIST:
                        return False
                else:
                    return False

        return True

    def restart(self) -> str:
        """Restart event."""
        err_msg = _("Only admin can restart bot!")
        if not self.__is_admin():
            return err_msg

        blank_data = {"persons": {}, "guests": {}}
        self.chat.participants = pickle_dumps(blank_data)
        self.chat.save()

        return _("The bot has been restarted.")

    def list_of_participants(self) -> str:
        """Get list of participants."""
        guests = False
        chat_members_str = _("Chat members")
        output = f"*{chat_members_str}:*"
        index = 1
        participants = pickle_loads(self.chat.participants)  # noqa: S301
        for person in participants["persons"].values():
            output += f"\n{index}. {person}"
            index += 1

        guest_index = 1
        for guest in participants["guests"].values():
            if not guests:
                guests_str = _("Guests:")
                output += f"\n\n *{guests_str}*"
                guests = True

            output += f"\n{guest_index}. {guest[Guests.NAME]} (+{guest[Guests.NUMBER]})"
            guest_index += 1

        total = self.get_total()
        total_str = _("Total")
        output += f"\n\n *{total_str}:* {total}"

        if total > 0:
            price = self.chat.price
            if price > 0:
                price_per_person = round(price / total)
                per_person_str = _("Price per person")
                output += f"\n\n *{per_person_str}:* {price_per_person}"

        return output

    def get_total(self) -> int:
        """Get total number of participants."""
        participants = pickle_loads(self.chat.participants)  # noqa: S301
        total = len(participants["persons"])
        for _person, guests in participants["guests"].items():
            total += guests[Guests.NUMBER]
        return total

    def add_person(self) -> str:
        """Add a person to event."""
        participants = pickle_loads(self.chat.participants)  # noqa: S301
        if self.person.id in participants["persons"]:
            duplicate_str = _("Duplicate found")
            already_str = _("already in the list")
            return f"{duplicate_str}: {self.person.person_str} {already_str}."

        total, err_msg = self.check_limit(add_number=1)
        if err_msg:
            return err_msg

        participants["persons"][self.person.id] = f"{self.person.person_str}"
        self.chat.participants = pickle_dumps(participants)
        self.chat.save()

        cool_str = _("Cool")
        accepted_str = _("accepted participation")
        we_are_str = _("Total participants")

        return f"{cool_str}! {self.person.person_str} {accepted_str}. {we_are_str} {total}!"

    def check_limit(self, add_number: int = 0) -> tuple:
        """Check the limit of participants."""
        total = self.get_total() + add_number
        limit = self.chat.limit
        error_str = _("Error")
        over_the_limit_str = _("You will go over the limit of participants")
        participants_str = _("participants")
        if total > limit:
            return (
                total,
                f"{error_str}. {over_the_limit_str} ({limit} {participants_str})",
            )

        return total, ""

    def remove_person(self) -> str:
        """Remove a person from the event."""
        participants = pickle_loads(self.chat.participants)  # noqa: S301
        if self.person.id in participants["persons"]:
            sorry_str = _("It is a pity that you can't")
            now_we_are_str = _("Now we are")
            participants["persons"].pop(self.person.id, None)
            self.chat.participants = pickle_dumps(participants)
            self.chat.save()
            total = self.get_total()
            return (
                f"\U0001F641 {sorry_str} "
                f"{self.person.person_str}. "
                f"{now_we_are_str} {total}"
            )
        cheater_str = _("Cheater")
        not_in_a_list_str = _("is not in a list")
        return f"{cheater_str} \U0001F47E! {self.person.person_str} {not_in_a_list_str}!"

    def add_guests(self, text) -> str:
        """Add guests."""
        guests = re.match(r"^\+(\d+)$", text)
        need_to_write_str = _("need to write")
        where_str = _("where")
        real_number_str = _("positive number from 1 till")
        if not guests:
            message_to_bot = (
                f"{self.person.person_str} {need_to_write_str} "
                f"[/+N]({BOT_LINK}), "
                f"{where_str} N - {real_number_str} {GUESTS_LIMIT}"
            )
            return message_to_bot

        number = int(guests.group(1))
        if number == 0 or number > GUESTS_LIMIT:
            message_to_bot = (
                f"{self.person.person_str} {need_to_write_str} "
                f"[/+N]({BOT_LINK}), "
                f"{where_str} N - {real_number_str} {GUESTS_LIMIT}"
            )
            return message_to_bot

        total, err_msg = self.check_limit(add_number=number)
        if err_msg:
            return err_msg

        participants = pickle_loads(self.chat.participants)  # noqa: S301
        guests_manager = participants["guests"].get(self.person.id)
        if not guests_manager:
            participants["guests"][self.person.id] = [
                self.person.person_str,
                number,
            ]
        else:
            current_value = guests_manager[Guests.NUMBER]
            if current_value + number > GUESTS_LIMIT:
                guests_limit_str = _("single player limit exceeded")
                limit_str = _("Limit")
                guests_str = _("guests")
                message_to_bot = (
                    f"{self.person.person_str}, {guests_limit_str}. "
                    f"{limit_str} - {GUESTS_LIMIT} {guests_str}"
                )
                return message_to_bot
            else:
                guests_manager[Guests.NUMBER] += number
                participants["guests"][self.person.id] = guests_manager

        self.chat.participants = pickle_dumps(participants)
        self.chat.save()
        welcome_guests_str = _("Well done! It's nice to see guests.")
        from_str = _("from")
        total_str = _("Total")
        message_to_bot = (
            f"{welcome_guests_str} "
            f"+{number} {from_str} {self.person.person_str}. "
            f"{total_str} {total}"
        )

        return message_to_bot

    def remove_guests(self, text) -> str:
        """Remove guests from the event."""
        remove_guests_str = re.match(r"^-(\d+)$", text)
        need_to_write_str = _("need to write")
        where_str = _("where")
        real_number_str = _("positive number from 1 till")
        if not remove_guests_str:
            message_to_bot = (
                f"{self.person.person_str} {need_to_write_str} "
                f"[/-N]({BOT_LINK}), "
                f"{where_str} N - {real_number_str} {GUESTS_LIMIT}"
            )
            return message_to_bot

        number = int(remove_guests_str.group(1))
        if number == 0 or number > GUESTS_LIMIT:
            message_to_bot = (
                f"{self.person.person_str} {need_to_write_str} "
                f"[/-N]({BOT_LINK}), "
                f"{where_str} N - {real_number_str} {GUESTS_LIMIT}"
            )
            return message_to_bot

        participants = pickle_loads(self.chat.participants)  # noqa: S301
        guests_manager = participants["guests"].get(self.person.id)
        if not guests_manager:
            cheater_str = _("Cheater")
            empty_guests_str = _("you haven't added anybody")
            message_to_bot = (
                f"{cheater_str} \U0001F47E! {self.person.person_str}, {empty_guests_str}."
            )
            return message_to_bot
        else:
            current_value = guests_manager[Guests.NUMBER]
            if current_value >= number:
                if current_value == number:
                    participants["guests"].pop(self.person.id, None)
                else:
                    guests_manager[Guests.NUMBER] -= number
                    participants["guests"][self.person.id] = guests_manager
            else:
                cheater_str = _("Cheater")
                over_the_limit_str = _("you try to remove more participants than added")
                fix_value_str = _("Fix the value")
                message_to_bot = (
                    f"{cheater_str} \U0001F47E! "
                    f"{self.person.person_str}, {over_the_limit_str}. "
                    f"{fix_value_str}."
                )
                return message_to_bot

        self.chat.participants = pickle_dumps(participants)
        self.chat.save()
        total = self.get_total()
        sorry_str = _("It is a pity to remove participants")
        from_str = _("from")
        now_we_are_str = _("Now we are")
        message_to_bot = (
            f"{sorry_str} \U0001F641\n"
            f"-{number} {from_str} {self.person.person_str}. "
            f"{now_we_are_str} {total}"
        )

        return message_to_bot

    def parse_set_methods(self, args: dict) -> str:
        """Parsing input settings."""
        for name, value in args.items():
            read_str = _("Read")
            if name == "price":
                if not self.set_price(value):
                    set_price_error_str = _("Wasn't able to set a correct price value")
                    return f"{set_price_error_str}. {read_str} [/help-set]({BOT_LINK})"
            elif name == "limit":
                if not self.set_limit(value):
                    set_limit_error_str = _(
                        "Wasn't able to set a correct limit of participants, "
                        "incorrect limit value"
                    )
                    return f"{set_limit_error_str}. {read_str} [/help-set]({BOT_LINK})"
            elif name == "language":
                if not self.set_language(value):
                    set_language_error_str = _("Wasn't able to set a correct language value")
                    return f"{set_language_error_str}. {read_str} [/help-set]({BOT_LINK})"
            else:
                error_str = _("Incorrect set command")
                return f"{error_str}. {read_str} [/help-set]({BOT_LINK})"

        correct_set_str = _("All settings were applied")
        return f"{correct_set_str}!"

    def set_price(self, value) -> bool:
        """Set price."""
        try:
            value = float(value)
        except ValueError:
            return False

        if value < 0:
            return False

        self.chat.price = value
        self.chat.save()

        return True

    def set_limit(self, value: int) -> bool:
        """Set limit."""
        try:
            value = int(value)
        except ValueError:
            return False

        if value < MIN_LIMIT or value > MAX_LIMIT:
            return False

        self.chat.limit = value
        self.chat.save()

        return True

    def set_language(self, value) -> bool:
        """Set language."""
        if language_exists(value):
            self.chat.language_code = value
            translation.activate(value)
            self.chat.save()

            return True

        return False

    def error_msg(self) -> str:
        """Build error message."""
        incorrect_value_str = _("you wright incorrect command")
        read_str = _("Read")
        return f"{self.person.person_str} {incorrect_value_str}. {read_str} [/help]({BOT_LINK})"

    def send_organizer_message(self, message):
        """Send a message to the chat."""
        data = {
            "chat_id": self.chat_id,
            "text": message,
            "parse_mode": "Markdown",
        }
        response = requests.post(
            f"{TELEGRAM_URL}{BOT_TEAM_ORGANIZER_TOKEN}/sendMessage", data=data, timeout=5
        )
        if response.status_code != 200:
            data["text"] = re.sub(r"_", "", message)
            response = requests.post(
                f"{TELEGRAM_URL}{BOT_TEAM_ORGANIZER_TOKEN}/sendMessage", data=data, timeout=5
            )
        if response.status_code != 200:
            data["parse_mode"] = "HTML"
            requests.post(
                f"{TELEGRAM_URL}{BOT_TEAM_ORGANIZER_TOKEN}/sendMessage", data=data, timeout=5
            )
