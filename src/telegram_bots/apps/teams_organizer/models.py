from django.db import models


class TeamOrganizerCollection(models.Model):
    """Team Organizer ORM."""

    chat_id = models.CharField(max_length=150, blank=False, null=False, unique=True, db_index=True)

    price = models.FloatField(default=0)
    participants = models.BinaryField()
    limit = models.SmallIntegerField(blank=False, null=False, default=15)
    language_code = models.CharField(default="en", max_length=5)

    class Meta:
        verbose_name = "Team Organizer Collection"
        verbose_name_plural = "Team Organizer Collections"
