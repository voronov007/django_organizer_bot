import json
import logging

from django.http import JsonResponse
from django.views import View
from telegram_bots.apps.teams_organizer.bot_methods import TeamOrganizer
from telegram_bots.apps.teams_organizer.utils.helpers import help_msg, help_set_msg
from telegram_bots.utils.text import args_handler

logger = logging.getLogger("team_organizer")


# https://api.telegram.org/bot<token>/setWebhook?url=<url>/webhook/team/team_organizer_bot_007/
class TeamsOrganizerView(View):
    """Team Organizer view class."""

    def post(self, request, *args, **kwargs) -> JsonResponse:
        """Read and process the chat message."""
        try:
            t_data = json.loads(request.body)
        except Exception as e:
            logger.error(f"Error during the message convert to JSON: {e}")
            return JsonResponse({"ok": "POST request processed with errors."})

        try:
            t_message = t_data["message"]
        except KeyError as e:
            logger.error(f"Cannot read the message: {e}. Data: {t_data}")
            return JsonResponse({"ok": "POST request processed with errors."})

        try:
            t_chat = t_message["chat"]
            text = t_message["text"].strip().lower()
            text = text.lstrip("/")
            chat = TeamOrganizer(t_chat, t_message["from"])
        except Exception as e:
            logger.error(f"Can't read the chat message: {e}")
            return JsonResponse({"ok": "POST request processed with errors."})

        try:
            self.process_message(text, chat)
        except Exception as e:
            logger.error(f"Can't process the message: {e}")
            try:
                chat.send_organizer_message(str(e))
            except Exception as e:
                logger.error(f"Can't send message ot the chat: {e}")
                chat.send_organizer_message("ERROR blyat")

        return JsonResponse({"ok": "POST request processed"})

    @staticmethod
    def process_message(text: str, chat: TeamOrganizer):
        """Process chat message."""
        if text == "+":
            message_to_bot = chat.add_person()
        elif text.startswith("+"):
            message_to_bot = chat.add_guests(text)
        elif text == "-":
            message_to_bot = chat.remove_person()
        elif text.startswith("-"):
            message_to_bot = chat.remove_guests(text)
        elif text == "help" or text == "start":
            message_to_bot = help_msg()
        elif text == "help-set" or text == "start":
            message_to_bot = help_set_msg()
        elif text == "list":
            message_to_bot = chat.list_of_participants()
        elif text == "restart" or text == "reset":
            message_to_bot = chat.restart()
        elif text.startswith("set"):
            args, error = args_handler("set", text)
            if error:
                message_to_bot = error
            else:
                message_to_bot = chat.parse_set_methods(args)
        else:
            message_to_bot = chat.error_msg()

        chat.send_organizer_message(message_to_bot)
