from django.utils.translation import gettext as _
from telegram_bots.apps.teams_organizer.constants import (
    BOT_LINK,
    GUESTS_LIMIT,
    MAX_LIMIT,
    MIN_LIMIT,
)

__all__ = ["help_msg", "help_set_msg"]


def help_msg() -> str:
    """Build help message."""
    confirm_str = _("Confirm participation")
    invite_guests_str = _("Invite guests")
    where_str = _("where")
    real_number_str = _("positive number from 1 till")
    cancel_participation_str = _("Cancel participation")
    remove_guests_str = _("Remove guests")
    general_info_str = _("general info")
    set_info_str = _("information on setting parameters")
    restart_str = _("restart event organization")
    text = (
        f"\U0001F449 *organizer_bot* \n"
        f"- {confirm_str}: [/+]({BOT_LINK}) \n"
        f"- {invite_guests_str}: [/+N]({BOT_LINK}),"
        f" {where_str} N - {real_number_str} {GUESTS_LIMIT}\n"
        f"- {cancel_participation_str}: [/-]({BOT_LINK}) \n"
        f"- {remove_guests_str}: [/-N]({BOT_LINK}), "
        f"{where_str} N - {real_number_str} {GUESTS_LIMIT}. \n\n"
        f"[/restart]({BOT_LINK}) - {restart_str}\n"
        f"[/help]({BOT_LINK}) - {general_info_str}\n"
        f"[/help-set]({BOT_LINK}) - {set_info_str}\n"
    )

    return text


def help_set_msg() -> str:
    """Build help set message."""
    set_arg_str = _("set to parameter")
    value_str = _("value")
    arguments_str = _("Arguments")
    set_lang_str = _("set language. Options are")
    set_total_price_str = _("set total price on event")
    any_value_str = _("any number more or equal to 0")
    set_limit_str = _("set limit of participants")
    real_number_from_str = _("real number from")
    till_str = _("till")
    examples_str = _("Examples")
    text = (
        f"\U0001F449 *organizer_bot* \n"
        f"[/set <arg>=<value>]({BOT_LINK}) {set_arg_str}(*arg*) "
        f"{value_str}(*value*)\n\n"
        f"*{arguments_str}* \n"
        f"- [language]({BOT_LINK}): {set_lang_str}: [en]({BOT_LINK}), "
        f"[ua]({BOT_LINK}), [ru]({BOT_LINK})\n"
        f"- [price]({BOT_LINK}): {set_total_price_str} "
        f"({any_value_str})\n"
        f"- [limit]({BOT_LINK}): {set_limit_str} ({real_number_from_str} "
        f"{MIN_LIMIT} {till_str} {MAX_LIMIT}). \n\n"
        f"{examples_str}:\n"
        f"[/set price=1234]({BOT_LINK})\n"
        f"[/set price=1234 limit=20 language=en]({BOT_LINK})\n"
    )

    return text
