from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _
from telegram_bots.apps.users.models import User


class UserCreationFormExtended(UserCreationForm):
    """User creation form class."""

    def __init__(self, *args, **kwargs):
        """Init email."""
        super(UserCreationFormExtended, self).__init__(*args, **kwargs)
        self.fields["email"] = forms.EmailField(label=_("E-mail"), max_length=75)


UserAdmin.add_form = UserCreationFormExtended
UserAdmin.add_fieldsets = (
    (
        None,
        {
            "classes": ("wide",),
            "fields": ("email", "username", "password1", "password2"),
        },
    ),
)

admin.site.register(User, UserAdmin)
