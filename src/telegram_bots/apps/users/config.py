from django.apps import AppConfig


class UserAppConfig(AppConfig):
    """User app config."""

    name = "telegram_bots.apps.users"
    label = "users"
    verbose_name = "Users"
