from django.contrib import sitemaps
from django.urls import reverse


class StaticViewSitemap(sitemaps.Sitemap):
    """Static view sitemap class."""

    priority = 0.5
    changefreq = "weekly"

    def items(self) -> list:
        """Get items in the statis view."""
        return ["about"]

    def location(self, item):
        """Reverse the string."""
        return reverse(item)


class DynamicViewSitemap(sitemaps.Sitemap):
    """Dynamic view sitemap."""

    priority = 0.9
    changefreq = "daily"

    def items(self) -> list:
        """Get list of dynamic views."""
        return [
            "charts:index",
            "charts:top-ru-music-videos",
            "charts:top-all-time-music-videos",
            "charts:top-movie-trailers",
        ]

    def location(self, item):
        """Reverse the string."""
        return reverse(item)
