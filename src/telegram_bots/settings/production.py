import os

import dj_database_url
from telegram_bots.settings.base import *

DEBUG = True if os.environ.get("DEBUG_ON_PRODUCTION") else False
ALLOWED_HOSTS.extend(
    [
        "127.0.0.1",
        "localhost",
        ".inmytop.com",
        "www.googletagmanager.com",
        "167.99.215.117",  # DigitalOcean server
    ]
)

CORS_ALLOWED_ORIGINS = [
    # "http://localhost:3000",
    # "http://127.0.0.1:3000",
    "https://inmytop.com",
    "https://www.inmytop.com",
    "https://host.inmytop.com",
    "https://www.host.inmytop.com",
]

MIDDLEWARE.insert(1, "whitenoise.middleware.WhiteNoiseMiddleware")

DATABASE_URL = os.environ.get("DATABASE_URL")
if DATABASE_URL:
    DATABASES = {"default": dj_database_url.parse(DATABASE_URL)}
else:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": os.environ.get("DATABASE_NAME", "sample_name"),
            "USER": os.environ.get("DATABASE_USER", "sample_user"),
            "PASSWORD": os.environ.get("DATABASE_PASSWORD", "sample_password"),
            # Or an IP Address that your DB is hosted on
            "HOST": os.environ.get("DATABASE_HOST", "localhost"),
            "PORT": os.environ.get("DATABASE_PORT", "5432"),
        }
    }

# Security https://docs.djangoproject.com/en/2.2/topics/security/#ssl-https
SECURE_PROXY_SSL_HEADER = ("HTTP_X_SITE_IS_SECURED", "https")

SECURE_SSL_REDIRECT = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
SECURE_HSTS_SECONDS = 31536000  # 1 year

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "{levelname} {asctime} module:[{module}] ProcessID:{process:d} {message}",  # NOQA: E501, B950
            "style": "{",
        },
        "simple": {
            "format": "{levelname} {asctime} [{module}] {message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "verbose"},
        "file": {
            "level": LOG_LEVEL,
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(BASE_DIR, "..", "..", "logs", "telegram_bots.log"),
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "verbose",
        },
    },
    "root": {"handlers": ["console", "file"], "level": LOG_LEVEL},
    "loggers": {
        "django": {
            "handlers": ["console", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        }
    },
}
