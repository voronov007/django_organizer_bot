def args_handler(prefix: str, message: str) -> tuple:
    """Handle arguments."""
    output = {}
    error = ""
    message = message.lstrip(prefix)
    args = message.split()
    for a in args:
        arg = a.split("=")
        if len(arg) == 2:
            output[arg[0]] = arg[1]
        else:
            error = "You are writing incorrect command. Read /help"
            break

    return output, error
