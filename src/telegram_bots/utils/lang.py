from django.conf import settings

languages = {el[0]: el[0] for el in settings.LANGUAGES}
languages["en"] = "en-us"


def get_default_language(lang_code: str) -> str:
    """Get default language."""
    return languages.get(lang_code, "en-us")


def language_exists(lang_code: str) -> bool:
    """Check if the language exists."""
    return languages.get(lang_code, False)
