import os

GOOGLE_API_KEY = os.environ.get("GOOGLE_API_KEY", "")
GOOGLE_API_KEY_2 = os.environ.get("GOOGLE_API_KEY_2", GOOGLE_API_KEY)
TELEGRAM_URL = "https://api.telegram.org/bot"
YOUTUBE_API_URL = "https://www.googleapis.com/youtube/v3/{method}"
YOUTUBE_API_WATCH_URL_FORMAT = "https://www.youtube.com/watch?v={id}"

BOT_TOP_CHARTS_TOKEN = os.getenv(
    # default value is not a correct key
    "BOT_TOP_CHARTS_TOKEN",
    "531314622:DAHNQeMKj6qpoUZNJavilaVwyghQtnS1aC5",
)

VIDEO_MONTHLY_CHART_START_RANGE = 5
VIDEO_MONTHLY_CHART_END_RANGE = 36
VIDEO_WEEKLY_CHART_START_RANGE = 21
VIDEO_WEEKLY_CHART_END_RANGE = 28
MUSIC_VIDEO_CATEGORY_ID = 10
TRAILER_VIDEO_CATEGORY_ID = 1  # Films and animation. (trailers id is 44)

VIDEOS_PER_PAGE = 10
VIDEOS_TO_SHOW = 30
VIEWS_THRESHOLD = 1_000_000  # 1 million
DAYS_IN_MONTH = 31

CHART_TASK_USERNAME = os.environ.get("CHART_TASK_USERNAME", "")
CHART_TASK_EMAIL = os.environ.get("CHART_TASK_EMAIL", "")

MIN_DAYS_PERIODIC_TASK_THRESHOLD = 27
WEEKLY_TOP_CHART_LIMIT = 10

WW_MUSIC_REQUEST_DATA = {
    "q": "official video, music  -india -china -korea -turkey -japan",
    "maxResults": 3,
    "regionCode": "US",
    "key": GOOGLE_API_KEY_2,
}
RU_MUSIC_REQUEST_DATA = {
    "q": "премьера, клип, видео",
    "order": "viewCount",
    "part": "snippet",
    "maxResults": 30,
    "videoCategoryId": MUSIC_VIDEO_CATEGORY_ID,
    "type": "video",
    "topicId": "/m/04rlf",
    "regionCode": "RU",
    "key": GOOGLE_API_KEY_2,
}
UA_MUSIC_REQUEST_DATA = {
    "q": "прем'єра, кліп, відео",
    "order": "viewCount",
    "part": "snippet",
    "maxResults": 30,
    "videoCategoryId": MUSIC_VIDEO_CATEGORY_ID,
    "type": "video",
    "topicId": "/m/04rlf",
    "regionCode": "UA",
    "key": GOOGLE_API_KEY_2,
}
TRAILERS_REQUEST_DATA = {
    "q": "trailer -india -china -korea -turkey -japan",
    "maxResults": 2,
    "regionCode": "US",
    "key": GOOGLE_API_KEY,
}
ALL_TIME_MUSIC_REQUEST_DATA = {
    "q": "official, music, video",
    "maxResults": VIDEOS_TO_SHOW,
    "regionCode": "US",
    "key": GOOGLE_API_KEY_2,
}

WEEKLY_WORLDWIDE_REQUEST_DATA = {
    "q": "official video, music  -india -china -korea -turkey -japan",
    "order": "viewCount",
    "part": "snippet",
    "maxResults": 30,
    "videoCategoryId": MUSIC_VIDEO_CATEGORY_ID,
    "type": "video",
    "topicId": "/m/04rlf",
    "regionCode": "US",
    "key": GOOGLE_API_KEY,
}
WEEKLY_RU_REQUEST_DATA = {
    "q": "премьера, клип, видео",
    "order": "viewCount",
    "part": "snippet",
    "maxResults": 30,
    "videoCategoryId": MUSIC_VIDEO_CATEGORY_ID,
    "type": "video",
    "topicId": "/m/04rlf",
    "regionCode": "RU",
    "key": GOOGLE_API_KEY,
}

WEEKLY_UA_REQUEST_DATA = {
    "q": "прем'єра, кліп, відео",
    "order": "viewCount",
    "part": "snippet",
    "maxResults": 30,
    "videoCategoryId": MUSIC_VIDEO_CATEGORY_ID,
    "type": "video",
    "topicId": "/m/04rlf",
    "regionCode": "UA",
    "key": GOOGLE_API_KEY,
}

BOOSTER_RANK_MULTIPLIER = 3
