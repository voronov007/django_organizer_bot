# InMyTop + Telegram bots

This project stores 2 applications:

- telegram bot to organize a team meetup
- app that builds YouTube charts periodically + bot for sending charts into channels

## Prerequisites:

- Python 3.11+
- Django 4.1+
- Docker + Docker Compose

## Run the app:

Next will install requirements into your virtual environment and run the project locally,
using SQL lite, NOT with the Docker

- Install requirements: `pip install -r requirements-dev`
- Run project: `make run`
- Set `GOOGLE_API_KEY` variable names (in sonstants)
- Set `BOT_TOP_CHARTS_TOKEN` variable name for handling chat reactions
  - call URI for transfering call on local machine: `https://api.telegram.org/bot{my_bot_token}/setWebhook?url={url_to_send_updates_to}`

## Run/Deploy with Docker:

- create `.env` and `docker/.env-vars` files.
  Example files were provided in the repo
- change Docker file to install develop requirements if needed
- set `LOCAL_DEV` env variable to `YES`: `LOCAL_DEV=YES`
- make commands are provided **NOT** for a docker, so look at init commands and run them manually

### DigitalOcean

#### Deploy

Deploy master branch to the production server, using git:

- `git remote add production <user>@<ip-address>:/var/repo/telegram_bots.git`
- `git push production master`

There is a git hook that will move code to the source code directory `/var/www/telegram_bots/`

Nginx setup:

- Config file:
  - `/etc/nging/sites-enabled/telegram_bots`
  - `/etc/nging/sites-enabled/react_app`

Settings. Example is for `inmytop.com` (frontend), but for the backend have created also `host.inmytop.com`:

```bash
upstream inmytop_server_com {
    server 127.0.0.1:3000;
}

server {
    server_name inmytop.com www.inmytop.com;

    if ($host = www.inmytop.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = inmytop.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;

    return 301 https://$host$request_uri;

}

server {
    listen 443 ssl http2;

    server_name inmytop.com www.inmytop.com;

    location / {
        proxy_set_header X-Site-Is-Secured https;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_ssl_session_reuse off;
        proxy_ssl_server_name on;
        proxy_pass http://inmytop_server_com;
    }

    ssl_certificate /etc/letsencrypt/live/inmytop.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/inmytop.com/privkey.pem; # managed by Certbot
}

```

- reload Nginx: `nginx -s reload`
- SSL cetrificate (https) is handled by Certbod (letsencrypt)
- cron jobs: `crontab -e`
- login into container: `docker compose exec -it app-fronend sh`

#### Run

- login `ssh <user>@<ip>`
- go to project dir `cd /var/www/telegram_bots/`
- stop containers: `docker-compose stop`
- remove containers: `docker0compose down`
- run & build: `docker-compose up -- build -d`. `-d` means to run in the background
  - run containers `docker-compose up -d`

## Reformat code:

- `make fmt`

## Check code lint:

- `make lint`

## Testing:

- `make test`

#### Troublshooting

- `sudo apt install gcc`
- `sudo apt-get install libpq-dev`
- `sudo apt install postgresql postgresql-contrib`
- `sudo apt-get install python<version>-dev`
