import "../App.css";

import React, { Component, Fragment } from "react";
import { fetchTopChartMusicJSON } from "../utils";
import Header from "../components/Header";
import Footer from "../components/Footer";
import TopChart from "../components/TopChart";

class UAandRUmusic extends Component {
  constructor(props) {
    super(props);
    this.pageHandler = this.pageHandler.bind(this);
    this.state = {
      chart_data: {},
    };
  }

  componentDidMount() {
    fetchTopChartMusicJSON("top-ru-music-videos/")
      .then((data) => this.setState({ chart_data: data }))
      .catch((error) => console.log("This is a custom error desct" + error));
  }

  pageHandler(page_number) {
    fetchTopChartMusicJSON("top-ru-music-videos/", page_number)
      .then((data) => {
        this.setState({ chart_data: data });
        window.scrollTo(0, 0);
      })
      .catch((error) => console.log("This is a custom error desct" + error));
  }

  render() {
    return (
      <Fragment>
        <Header />
        <div className="container">
          <TopChart
            data={this.state.chart_data}
            pageHandler={this.pageHandler}
          />
          <Footer />
        </div>
      </Fragment>
    );
  }
}

export default UAandRUmusic;
