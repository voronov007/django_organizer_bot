import "../App.css";

import React, { Component, Fragment } from "react";
import Header from "../components/Header";
import Logo from "../components/Logo";
import Footer from "../components/Footer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInstagram,
  faLinkedin,
  faTelegramPlane,
} from "@fortawesome/free-brands-svg-icons";
import { faExternalLinkAlt, faLink } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";

class About extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <div className="container">
          <Logo />
          <div className="row text">
            <div className="col-12">
              <p className="lead">
                This web site allows you to view{" "}
                <strong className="text-solid">Top YouTube Video Charts</strong>{" "}
                for free. Why have I created this project? Well, I had an idea
                to lead an own blog in{" "}
                <NavLink
                  to="https://t.me/dude007_channel"
                  className="link-solid"
                  rel="noopener noreferrer"
                  target="_blank"
                  aria-label="Telegram Channel"
                >
                  {"Telegram Channel "}
                  <FontAwesomeIcon icon={faTelegramPlane} />{" "}
                  <FontAwesomeIcon icon={faExternalLinkAlt} />{" "}
                </NavLink>
                . The blog about IT, parties, life and life hacks etc. I didn't
                want to limit myself with one subject, but also I didn't want to
                write posts about every move I do during the day. I realized
                that sometimes I might have no time to write posts or might not
                have good mood for it, but still I would like to be in contact
                with you, subscriber. For this purpose I have created a Telegram
                Bot which every week makes a post with a list of{" "}
                <strong className="text-solid">Weekly Top Video Chart</strong>.
                So, even if there are no posts from me, you get a weekly{" "}
                <strong className="text-solid">YouTube Music Videos</strong>{" "}
                chart updates. Subscribe to my{" "}
                <NavLink
                  to="https://t.me/dude007_channel"
                  className="link-solid"
                  rel="noopener noreferrer"
                  target="_blank"
                  aria-label="Telegram Channel"
                >
                  {"Telegram Channel "}
                  <FontAwesomeIcon icon={faTelegramPlane} />{" "}
                  <FontAwesomeIcon icon={faExternalLinkAlt} />{" "}
                </NavLink>{" "}
                I think you'll like it.
              </p>
              <p className="lead">
                I was need to practice on creating a{" "}
                <strong className="text-solid">Top YouTube Video Charts</strong>{" "}
                and view results and so I created this web site. Also I was
                curious which{" "}
                <strong className="text-solid">YouTube Music Videos</strong> are
                most viewed?! There are several web sites that show such info
                but all of them have lots of advertisements, haven't been
                updated for a long time and have only few videos in the chart. I
                have decided to fix it and so created a{" "}
                <NavLink
                  to="/top-all-time-music-videos"
                  className="link-solid"
                  aria-label="All Time Music Videos"
                >
                  {"All Time Music Videos "}
                  <FontAwesomeIcon icon={faLink} />{" "}
                </NavLink>{" "}
                chart on my web site too. At some point I was not able to stop
                myself in developing the web site as it was fun and challenging.
                On this project I have practiced a few new technologies that I
                haven't used before, for example,{" "}
                <strong className="text-solid">
                  Progressive Web Application (PWA)
                </strong>{" "}
                and multi threading. Thanks{" "}
                <NavLink
                  to="https://www.linkedin.com/in/olha-voronova/"
                  className="link-solid"
                  aria-label="Olha Voronova"
                >
                  {"Olha "}
                  <FontAwesomeIcon icon={faLinkedin} />{" "}
                  <FontAwesomeIcon icon={faExternalLinkAlt} />{" "}
                </NavLink>{" "}
                for the web design and logo.
              </p>
              <p className="lead">
                I generate{" "}
                <NavLink
                  to="/"
                  className="link-solid"
                  aria-label="YouTube Video Charts"
                >
                  {"YouTube Video Charts "}
                  <FontAwesomeIcon icon={faLink} />{" "}
                </NavLink>{" "}
                once per month in different dates as YouTube limits daily API
                requests. TopChart algorithm is based on total views and date
                when video was published and that is why sometimes video may
                have more views but stay lower than video with less views. Hope
                you will enjoy the website as much as I do. Thanks for viewing
                the web site. You may contact me via{" "}
                <NavLink
                  to="https://www.linkedin.com/in/anvoronov/?locale=en_US"
                  className="link-solid"
                  aria-label="Andrii Voronov"
                >
                  {"LinkedId "}
                  <FontAwesomeIcon icon={faLinkedin} />{" "}
                  <FontAwesomeIcon icon={faExternalLinkAlt} />{" "}
                </NavLink>{" "}
                and/or subscribe on my{" "}
                <NavLink
                  to="https://t.me/dude007_channel"
                  className="link-solid"
                  rel="noopener noreferrer"
                  target="_blank"
                  aria-label="Telegram Channel"
                >
                  {"Telegram Channel "}
                  <FontAwesomeIcon icon={faTelegramPlane} />{" "}
                  <FontAwesomeIcon icon={faExternalLinkAlt} />{" "}
                </NavLink>{" "}
                and{" "}
                <NavLink
                  to="https://www.instagram.com/voronov.a.g/"
                  className="link-solid"
                  rel="noopener noreferrer"
                  target="_blank"
                  aria-label="Instagram"
                >
                  {"Instagram "}
                  <FontAwesomeIcon icon={faInstagram} />{" "}
                  <FontAwesomeIcon icon={faExternalLinkAlt} />{" "}
                </NavLink>
              </p>
              <p className="lead text-end">Best regards, Andrii</p>
            </div>
          </div>
          <Footer />
        </div>
      </Fragment>
    );
  }
}

export default About;
