import "../App.css";

import React, { Component, Fragment } from "react";
import { fetchTopChartMusicJSON } from "../utils";
import Header from "../components/Header";
import Footer from "../components/Footer";
import TopChart from "../components/TopChart";

class WorldwideMusic extends Component {
  constructor(props) {
    super(props);
    this.pageHandler = this.pageHandler.bind(this);
    this.state = {
      chart_data: {},
      color: "red",
    };
  }

  componentDidMount() {
    // console.log("The state is:");
    // console.log(this.state);
    fetchTopChartMusicJSON()
      .then((data) => this.setState({ chart_data: data }))
      .catch((error) => console.log("This is a custom error desct" + error));
  }

  pageHandler(page_number) {
    fetchTopChartMusicJSON("", page_number)
      .then((data) => {
        this.setState({ chart_data: data });
        window.scrollTo(0, 0);
      })
      .catch((error) => console.log("This is a custom error desct" + error));
  }

  render() {
    return (
      <Fragment>
        <Header />
        <div className="container">
          <TopChart
            data={this.state.chart_data}
            pageHandler={this.pageHandler}
          />
          <Footer />
        </div>
      </Fragment>
    );
  }
}

export default WorldwideMusic;
