import { BASE_URL } from "./constants/constants.js";

async function fetchTopChartMusicJSON(url = "", pageNumber = "") {
  if (pageNumber !== "") {
    url += "?page=" + pageNumber;
  }
  const response = await fetch(BASE_URL + url);

  if (!response.ok) {
    const message = `An error has occured: ${response.status}`;
    console.log("This is another error " + message);
  }
  //   response.status;

  const chart_data = await response.json();
  // console.log("The data is:");
  // console.log(chart_data);

  return chart_data;
}

export { fetchTopChartMusicJSON };
