import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./index.css";
import About from "./pages/About";
import WordwideMusic from "./pages/WorldMusic";
import UAandRUmusic from "./pages/UAandRUmusic";
import AllTimeMusic from "./pages/AllTimeMusic";
import MovieTrailers from "./pages/MovieTrailers";
import reportWebVitals from "./reportWebVitals";
import ReactGA from "react-ga4";

const root = ReactDOM.createRoot(document.getElementById("root"));
ReactGA.initialize("UA-27163435-8");
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/about" element={<About />} />
        <Route path="/" element={<WordwideMusic />} />
        <Route path="/top-ua-and-ru-music" element={<UAandRUmusic />} />
        <Route path="/top-all-time-music-videos" element={<AllTimeMusic />} />
        <Route path="/top-movie-trailers" element={<MovieTrailers />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
const SendAnalytics = () => {
  ReactGA.send({
    hitType: "pageview",
    page: window.location.pathname,
  });
};

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(SendAnalytics);
