import React from "react";

function Logo() {
  return (
    <div className="chart-title">
      <br />
      <br />
      <br />
      <div className="row">
        <div className="col-12">
          <a href="#">
            <img
              src="images/icons/logo-with-text.svg"
              className="card-img-top"
              alt="Logo"
            />
          </a>
        </div>
      </div>
      <br />
      <br />
    </div>
  );
}

export default Logo;
