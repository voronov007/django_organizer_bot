import "../App.css";

import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { faYoutube } from "@fortawesome/free-brands-svg-icons";
import {
  faUpload,
  faChartLine,
  faEye,
  faThumbsUp,
  faThumbsDown,
  faExternalLinkSquareAlt,
} from "@fortawesome/free-solid-svg-icons";

class TopChart extends Component {
  getPagesContent(pages) {
    let content = [];
    if (pages.num_pages === 1) {
      return content;
    }
    for (let page_number = 1; page_number <= pages.num_pages; page_number++) {
      content.push(
        <button
          href={"?page=" + page_number}
          aria-label="Page"
          key={"page-" + page_number}
          className={
            "btn btn-outline-secondary " +
            (pages.current_page === page_number ? "active" : "")
          }
          onClick={() => this.props.pageHandler(page_number)}
        >
          {" " + page_number + " "}
        </button>
      );
    }
    return content;
  }

  render() {
    return (
      <Fragment>
        <h1 className="text-center chart-title">{this.props.data.top_title}</h1>
        <h2 className="text-center">
          {this.props.data.chart
            ? "Generated: " + this.props.data.chart.name + " "
            : "Loading"}
          <a
            href="#collapseExample"
            aria-label="Collapsed text"
            data-bs-toggle="collapse"
            data-bs-target="#collapseExample"
            aria-expanded="false"
            aria-controls="collapseExample"
          >
            <FontAwesomeIcon icon={faQuestionCircle} />
          </a>
        </h2>

        <div className="row">
          <div className="col-12">
            <div className="collapse" id="collapseExample">
              <div className="alert alert-light" role="alert">
                {this.props.data.help_text}
              </div>
            </div>
          </div>
        </div>

        {this.props.data.items ? (
          this.props.data.items.map((item) => (
            <div className="video-item" key={item.id}>
              <div className="row">
                <div className="col-12">
                  <h4>
                    <span className="badge badge-dark">{item.position}</span>{" "}
                    {item.title}
                  </h4>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="videoWrapper ratio ratio-16x9">
                    <iframe
                      src={
                        "https://www.youtube-nocookie.com/embed/" +
                        item.video_id
                      }
                      title={item.title}
                      frameBorder="0"
                      allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                    ></iframe>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 text-end video-statistics">
                  <button
                    type="button"
                    className="btn btn-outline-dark"
                    disabled
                  >
                    <FontAwesomeIcon icon={faUpload} />{" "}
                    <span className="badge badge-light">
                      {item.published_at}
                    </span>
                  </button>
                  <button
                    type="button"
                    className="btn btn-outline-dark"
                    disabled
                  >
                    <FontAwesomeIcon icon={faChartLine} />{" "}
                    <span className="badge badge-light">{item.rank_str}</span>
                  </button>
                  <button
                    type="button"
                    className="btn btn-outline-dark"
                    disabled
                  >
                    <FontAwesomeIcon icon={faEye} />{" "}
                    <span className="badge badge-light">{item.views_str}</span>
                  </button>
                  <button
                    type="button"
                    className="btn btn-outline-dark"
                    disabled
                  >
                    <FontAwesomeIcon icon={faThumbsUp} />{" "}
                    <span className="badge badge-light">{item.likes_str}</span>
                  </button>
                  <a
                    className="btn btn-outline-dark"
                    href={item.url}
                    aria-label={"Watch on YouTube " + item.title}
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faExternalLinkSquareAlt} />
                    Watch <FontAwesomeIcon icon={faYoutube} />
                  </a>
                </div>
              </div>
            </div>
          ))
        ) : (
          <div>No data</div>
        )}

        <p className="text-center step-links">
          {this.props.data.pages
            ? this.getPagesContent(this.props.data.pages)
            : ""}
        </p>
      </Fragment>
    );
  }
}

export default TopChart;
