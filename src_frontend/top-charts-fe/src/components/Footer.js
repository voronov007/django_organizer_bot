import React, { Component } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInstagram,
  faLinkedin,
  faTelegramPlane,
} from "@fortawesome/free-brands-svg-icons";
import { faAddressCard } from "@fortawesome/free-regular-svg-icons";

class Fooeter extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand navbar-dark bg-solid">
        <div className="container">
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav w-100 nav-justified">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  href="/about"
                  rel="noreferrer noopener"
                  aria-label="About Top Charts project"
                >
                  <FontAwesomeIcon icon={faAddressCard} size="2x" />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active"
                  href="https://t.me/dude007_channel"
                  rel="noreferrer noopener"
                  target="_blank"
                  aria-label="Telegram channel dude007"
                >
                  <FontAwesomeIcon icon={faTelegramPlane} size="2x" />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active"
                  href="https://www.linkedin.com/in/anvoronov/?locale=en_US"
                  rel="noreferrer noopener"
                  target="_blank"
                  aria-label="LinkedIn"
                >
                  <FontAwesomeIcon icon={faLinkedin} size="2x" />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active"
                  href="https://www.instagram.com/voronov.a.g/"
                  aria-label="Subscribe to my Instagram account"
                  rel="noreferrer noopener"
                  target="_blank"
                >
                  <FontAwesomeIcon icon={faInstagram} size="2x" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Fooeter;
