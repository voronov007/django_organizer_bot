import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Header extends Component {
  render() {
    return (
      <div className="container">
        <div>
          <nav className="navbar navbar-expand-lg navbar-dark bg-solid fixed-top">
            <div className="container">
              <a className="navbar-brand" href="/" aria-label="Top Charts Logo">
                <img
                  src="images/icons/logo-with-text.svg"
                  alt="Top Charts"
                  height="40px"
                />
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item active">
                    <NavLink to="/" className="nav-link">
                      Worldwide Music
                    </NavLink>
                  </li>
                  <li className="nav-item invisible-on-medium-down">
                    <a className="nav-link" href="#" aria-label="Separator">
                      |
                    </a>
                  </li>
                  <li className="nav-item">
                    <NavLink to="/top-ua-and-ru-music" className="nav-link">
                      UA & RU Music
                    </NavLink>
                  </li>
                  <li className="nav-item invisible-on-medium-down">
                    <a className="nav-link" href="#" aria-label="Separator">
                      |
                    </a>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      to="/top-all-time-music-videos"
                      className="nav-link"
                    >
                      All Time Music
                    </NavLink>
                  </li>
                  <li className="nav-item invisible-on-medium-down">
                    <a className="nav-link" href="#" aria-label="Separator">
                      |
                    </a>
                  </li>
                  <li className="nav-item">
                    <NavLink to="/top-movie-trailers" className="nav-link">
                      Movie Trailers
                    </NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    );
  }
}

export default Header;
