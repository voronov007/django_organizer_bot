export const BASE_URL = process.env.REACT_APP_HOST_IP_ADDRESS
  ? "https://" + process.env.REACT_APP_HOST_IP_ADDRESS + "/"
  : "http://localhost:6100/";
