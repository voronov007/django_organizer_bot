import pickle  # noqa: S403

import pytest
from telegram_bots.apps.teams_organizer.bot_methods import TeamOrganizer
from telegram_bots.apps.teams_organizer.models import TeamOrganizerCollection


@pytest.mark.django_db
class TestTeamsOrganizer:
    def test_teams_organizer_new_chat(self):
        chat_id = "123_chat"
        participants = {"persons": {}, "guests": {}}
        pickled_data = pickle.dumps(participants)
        new_chat = TeamOrganizerCollection.objects.create(
            chat_id=chat_id, participants=pickled_data
        )

        assert new_chat.price == 0
        assert True

    def test_person(self):
        assert TeamOrganizerCollection.objects.count() == 0
        t_chat = {"id": "test1"}
        message = {
            "from": {
                "language_code": "en",
                "id": "person_1",
                "first_name": "TestName",
                "last_name": "TestSurname",
            }
        }
        organizer = TeamOrganizer(t_chat=t_chat, person_data=message["from"])
        organizer.add_person()

        chat = TeamOrganizerCollection.objects.filter(chat_id="test1").first()
        participants = pickle.loads(chat.participants)  # noqa: S301
        assert "person_1" in participants["persons"]

        organizer = TeamOrganizer(t_chat=t_chat, person_data=message["from"])
        organizer.remove_person()

        chat = TeamOrganizerCollection.objects.filter(chat_id="test1").first()
        participants = pickle.loads(chat.participants)  # noqa: S301
        assert "person_1" not in participants["persons"]

    def test_guests(self):
        assert TeamOrganizerCollection.objects.count() == 0
        t_chat = {"id": "test1"}
        message = {
            "from": {
                "language_code": "en",
                "id": "person_1",
                "first_name": "TestName",
                "last_name": "TestSurname",
            }
        }
        organizer = TeamOrganizer(t_chat=t_chat, person_data=message["from"])
        organizer.add_guests("+3")

        chat = TeamOrganizerCollection.objects.filter(chat_id="test1").first()
        participants = pickle.loads(chat.participants)  # noqa: S301
        assert not participants["persons"]
        assert participants["guests"]["person_1"][1] == 3

        organizer = TeamOrganizer(t_chat=t_chat, person_data=message["from"])
        organizer.remove_guests("-2")
        chat = TeamOrganizerCollection.objects.filter(chat_id="test1").first()
        participants = pickle.loads(chat.participants)  # noqa: S301
        assert not participants["persons"]
        assert participants["guests"]["person_1"][1] == 1

    def test_list_of_participants(self):
        t_chat = {"id": "test1"}
        message = {
            "from": {
                "language_code": "en",
                "id": "person_1",
                "first_name": "TestName",
                "last_name": "TestSurname",
            }
        }
        organizer = TeamOrganizer(t_chat=t_chat, person_data=message["from"])
        organizer.add_person()
        organizer.add_guests("+3")
        msg = organizer.list_of_participants()

        # 3 + 1
        assert "4" in msg
