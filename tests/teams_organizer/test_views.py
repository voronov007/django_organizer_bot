import pytest
from django.test import Client
from telegram_bots.apps.teams_organizer.bot_methods import TeamOrganizer
from telegram_bots.apps.teams_organizer.views import TeamsOrganizerView


@pytest.mark.django_db
class TestTeamsOrganizerView:
    def setup_method(self):
        self.client = Client()

    def test_incorrect_json(self):
        response = self.client.post(
            "/webhook/team/team_organizer_bot_007/", "aaaa", content_type="application/json"
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed with errors."

    def test_no_message(self):
        response = self.client.post(
            "/webhook/team/team_organizer_bot_007/",
            {"data": "test"},
            content_type="application/json",
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed with errors."

    def test_read_message_error(self):
        response = self.client.post(
            "/webhook/team/team_organizer_bot_007/",
            {"message": {"chat": 123, "text": "/test"}},
            content_type="application/json",
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed with errors."

    def test_read_message(self, mocker):
        post_request = mocker.patch("telegram_bots.apps.teams_organizer.bot_methods.requests.post")
        message = {
            "chat": {"id": 123},
            "text": "/test",
            "from": {"id": 1, "username": "test", "first_name": "John", "last_name": "Travolta"},
        }
        response = self.client.post(
            "/webhook/team/team_organizer_bot_007/",
            {"message": message},
            content_type="application/json",
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed"
        assert post_request.call_count == 3

    def test_process_message(self, mocker):
        patched_method = mocker.patch.object(
            TeamOrganizer, "send_organizer_message", return_value=None
        )

        message = {
            "chat": {"id": 123, "type": "test"},
            "text": "/test",
            "from": {"id": 1, "username": "test", "first_name": "John", "last_name": "Travolta"},
        }
        chat = TeamOrganizer(t_chat=message["chat"], person_data=message["from"])
        TeamsOrganizerView.process_message("+", chat)
        TeamsOrganizerView.process_message("-", chat)
        TeamsOrganizerView.process_message("+1", chat)
        TeamsOrganizerView.process_message("-1", chat)
        TeamsOrganizerView.process_message("help", chat)
        TeamsOrganizerView.process_message("help-set", chat)
        TeamsOrganizerView.process_message("list", chat)
        TeamsOrganizerView.process_message("restart", chat)
        TeamsOrganizerView.process_message("set 1", chat)
        TeamsOrganizerView.process_message("set price=1500", chat)

        assert patched_method.call_count == 10
