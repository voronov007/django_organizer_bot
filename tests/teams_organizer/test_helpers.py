from telegram_bots.apps.teams_organizer.utils.helpers import help_msg, help_set_msg


def test_help_msgs():
    assert help_msg()
    assert help_set_msg()
