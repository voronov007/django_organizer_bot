from random import choice, randint

import faker
from django.utils import timezone as tz
from factory import Sequence, SubFactory
from factory.django import DjangoModelFactory
from telegram_bots.apps.top_charts.models import YouTubeChart, YouTubeVideo

__all__ = ["YouTubeChartFactory", "YouTubeVideoFactory"]


fake = faker.Factory.create()


class YouTubeChartFactory(DjangoModelFactory):
    class Meta:
        model = YouTubeChart
        django_get_or_create = ("date", "chart_type")

    name = Sequence(lambda m: fake.name()[:50])
    date = Sequence(lambda m: tz.now().date())
    chart_type = Sequence(lambda m: choice(YouTubeChart.CHART_TYPE_CHOICES)[0])  # noqa: S311


class YouTubeVideoFactory(DjangoModelFactory):
    class Meta:
        model = YouTubeVideo

    chart = SubFactory(YouTubeChartFactory)

    published_at = Sequence(lambda m: tz.now().date())
    position = Sequence(lambda m: m + 1)
    video_id = Sequence(lambda m: m + 1)
    title = Sequence(lambda m: fake.word()[:50])
    url = Sequence(lambda m: f"https://example-{m}.com")
    vpd = Sequence(lambda m: randint(0, 9999))  # noqa: S311
    views_str = Sequence(lambda m: f"{randint(0, 9999)}m")  # noqa: S311
    likes_str = Sequence(lambda m: f"{randint(0, 9999)}m")  # noqa: S311
    dislikes_str = Sequence(lambda m: f"{randint(0, 9999)}m")  # noqa: S311
    views_num = Sequence(lambda m: randint(0, 9999))  # noqa: S311
    likes_num = Sequence(lambda m: randint(0, 9999))  # noqa: S311
    dislikes_num = Sequence(lambda m: randint(0, 9999))  # noqa: S311
