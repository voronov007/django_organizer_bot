import faker
from factory import Sequence
from factory.django import DjangoModelFactory
from telegram_bots.apps.users.models import User

__all__ = ["UserFactory"]


fake = faker.Factory.create()


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    username = Sequence(lambda u: fake.user_name())
    password = Sequence(lambda u: fake.password())
    email = Sequence(lambda u: fake.email())
