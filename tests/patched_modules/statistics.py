import datetime as dt
import json
from random import randint

from telegram_bots.constants import VIDEOS_TO_SHOW


class PatchedQueue:
    def get(self):
        return ""

    def put(self, value):
        pass

    def task_done(self):
        pass

    def join(self):
        pass


class PatchedYTSearchResponse:
    content = json.dumps(
        {
            "items": [
                {
                    "id": {"videoId": 1111},
                    "snippet": {
                        "title": "Item 1",
                        "publishedAt": dt.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                    },
                }
            ]
        }
    )
    text = "Some text"

    def __init__(self, status_code=200):
        self.status_code = status_code

    @property
    def dict_content(self):
        return json.loads(self.content)


class PatchedYTVideosResponse:
    content = json.dumps(
        {
            "items": [
                {
                    "id": {"videoId": 1111},
                    "statistics": {
                        "viewCount": "1234",
                        "likeCount": "567",
                        "dislikeCount": "0",
                    },
                }
            ]
        }
    )
    text = "Some text"

    def __init__(self, status_code=200):
        self.status_code = status_code

    @property
    def dict_content(self):
        return json.loads(self.content)


class PatchedThread:
    def __init__(self, target, args, daemon=False):
        self.target = target
        self.args = args
        self.daemon = daemon

    def setDaemon(self, value):
        pass

    def start(self):
        pass


def patched_yt_search_request_get_401(url, params=None, timeout=1):
    return PatchedYTSearchResponse(status_code=401)


def patched_yt_search_request_get_200(url, params=None, timeout=1):
    return PatchedYTSearchResponse(status_code=200)


def patched_yt_videos_request_get_401(url, params=None, timeout=1):
    return PatchedYTVideosResponse(status_code=401)


def patched_yt_videos_request_get_200(url, params=None, timeout=1):
    return PatchedYTVideosResponse(status_code=200)


def patched_get_chart_pass_threshold(r_params, result_list, q):
    # pass threshold
    for i in range(VIDEOS_TO_SHOW):
        result_list.append(
            {
                "video_id": f"video-{i}",
                "title": f"Title {i}",
                "url": f"https://example.com/{i}",
                "rank": randint(0, 1000),  # noqa: S311
                "rank_str": f"{randint(1,9)}k",  # noqa: S311
                "published_at": dt.datetime.now().date(),
                "statistics": {
                    "views": f"{randint(1,9)}k",  # noqa: S311
                    "likes": f"{randint(1,9)}k",  # noqa: S311
                    "dislikes": f"{randint(1,9)}k",  # noqa: S311
                    "views_count": randint(1000000, 90000000),  # noqa: S311
                    "likes_count": randint(0, 100),  # noqa: S311
                    "dislikes_count": randint(0, 100),  # noqa: S311
                    "vpd": randint(0, 1000),  # noqa: S311
                    "vpd_str": f"{randint(1,9)}k",  # noqa: S311
                },
            }
        )

    # not pass threshold
    for i in range(5):
        result_list.append(
            {
                "video_id": f"video-{i}",
                "title": f"Title {i}",
                "published_at": dt.datetime.now().date(),
                "url": f"https://example.com/{i}",
                "rank": randint(0, 1000),  # noqa: S311
                "rank_str": f"{randint(1, 9)}k",  # noqa: S311
                "statistics": {
                    "views": f"{randint(1, 9)}k",  # noqa: S311
                    "likes": f"{randint(1, 9)}k",  # noqa: S311
                    "dislikes": f"{randint(1, 9)}k",  # noqa: S311
                    "views_count": randint(1, 100),  # noqa: S311, low views
                    "likes_count": randint(0, 100),  # noqa: S311
                    "dislikes_count": randint(0, 100),  # noqa: S311
                    "vpd": randint(0, 1000),  # noqa: S311
                    "vpd_str": f"{randint(1, 9)}k",  # noqa: S311
                },
            }
        )


def patched_run_get_chart_worker(
    request_params, result, dates_params, q, q_value, booster_rank=False
):
    # pass threshold
    for i in range(VIDEOS_TO_SHOW):
        result.append(
            {
                "video_id": f"video-{i}",
                "title": f"Title {i}",
                "url": f"https://example.com/{i}",
                "rank": randint(0, 1000),  # noqa: S311
                "rank_str": f"{randint(1,9)}k",  # noqa: S311
                "published_at": dt.datetime.now().date(),
                "statistics": {
                    "views": f"{randint(1,9)}k",  # noqa: S311
                    "likes": f"{randint(1,9)}k",  # noqa: S311
                    "dislikes": f"{randint(1,9)}k",  # noqa: S311
                    "views_count": randint(1000000, 90000000),  # noqa: S311
                    "likes_count": randint(0, 100),  # noqa: S311
                    "dislikes_count": randint(0, 100),  # noqa: S311
                    "vpd": randint(0, 1000),  # noqa: S311
                    "vpd_str": f"{randint(1,9)}k",  # noqa: S311
                },
            }
        )

    # not pass threshold
    for i in range(5):
        result.append(
            {
                "video_id": f"video-{i}",
                "title": f"Title {i}",
                "published_at": dt.datetime.now().date(),
                "url": f"https://example.com/{i}",
                "rank": randint(0, 1000),  # noqa: S311
                "rank_str": f"{randint(1, 9)}k",  # noqa: S311
                "statistics": {
                    "views": f"{randint(1, 9)}k",  # noqa: S311
                    "likes": f"{randint(1, 9)}k",  # noqa: S311
                    "dislikes": f"{randint(1, 9)}k",  # noqa: S311
                    "views_count": randint(1, 100),  # noqa: S311, low views
                    "likes_count": randint(0, 100),  # noqa: S311
                    "dislikes_count": randint(0, 100),  # noqa: S311
                    "vpd": randint(0, 1000),  # noqa: S311
                    "vpd_str": f"{randint(1, 9)}k",  # noqa: S311
                },
            }
        )


def patched_get_chart_less_threshold(r_params, result_list, q):
    for i in range(VIDEOS_TO_SHOW - 5):
        result_list.append(
            {
                "video_id": f"video-{i}",
                "title": f"Title {i}",
                "published_at": dt.datetime.now().date(),
                "url": f"https://example.com/{i}",
                "rank": randint(0, 1000),  # noqa: S311
                "rank_str": f"{randint(1, 9)}k",  # noqa: S311
                "statistics": {
                    "views": f"{randint(1, 9)}k",  # noqa: S311
                    "likes": f"{randint(1, 9)}k",  # noqa: S311
                    "dislikes": f"{randint(1, 9)}k",  # noqa: S311
                    "views_count": randint(1000000, 90000000),  # noqa: S311
                    "likes_count": randint(0, 100),  # noqa: S311
                    "dislikes_count": randint(0, 100),  # noqa: S311
                    "vpd": randint(0, 1000),  # noqa: S311
                    "vpd_str": f"{randint(1, 9)}k",  # noqa: S311
                },
            }
        )

    for i in range(5):
        result_list.append(
            {
                "video_id": f"video-{i}",
                "title": f"Title {i}",
                "published_at": dt.datetime.now().date(),
                "url": f"https://example.com/{i}",
                "rank": randint(0, 1000),  # noqa: S311
                "rank_str": f"{randint(1, 9)}k",  # noqa: S311
                "statistics": {
                    "views": f"{randint(1, 9)}k",  # noqa: S311
                    "likes": f"{randint(1, 9)}k",  # noqa: S311
                    "dislikes": f"{randint(1, 9)}k",  # noqa: S311
                    "views_count": randint(1, 100),  # noqa: S311, low views
                    "likes_count": randint(0, 100),  # noqa: S311
                    "dislikes_count": randint(0, 100),  # noqa: S311
                    "vpd": randint(0, 1000),  # noqa: S311
                    "vpd_str": f"{randint(1, 9)}k",  # noqa: S311
                },
            }
        )


def patched_get_all_time_chart(r_params, result_list):
    for i in range(VIDEOS_TO_SHOW):
        result_list.append(
            {
                "video_id": f"video-{i}",
                "title": f"Title {i}",
                "published_at": dt.datetime.now().date(),
                "url": f"https://example.com/{i}",
                "rank": randint(0, 1000),  # noqa: S311
                "rank_str": f"{randint(1, 9)}k",  # noqa: S311
                "statistics": {
                    "views": f"{randint(1, 9)}k",  # noqa: S311
                    "likes": f"{randint(1, 9)}k",  # noqa: S311
                    "dislikes": f"{randint(1, 9)}k",  # noqa: S311
                    "views_count": randint(1, 100),  # noqa: S311
                    "likes_count": randint(0, 100),  # noqa: S311
                    "dislikes_count": randint(0, 100),  # noqa: S311
                    "vpd": randint(0, 1000),  # noqa: S311
                    "vpd_str": f"{randint(1, 9)}k",  # noqa: S311
                },
            }
        )
