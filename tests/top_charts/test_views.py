import pytest
from django.test import RequestFactory
from django.utils import timezone as tz
from telegram_bots.apps.top_charts.models import YouTubeChart
from telegram_bots.apps.top_charts.views import (
    ATMusicVideoChartView,
    PeriodicMonthlyChartTaskView,
    RUMusicVideoChartView,
    TrailersChartView,
    WWMusicVideoChartView,
)
from telegram_bots.constants import VIDEOS_PER_PAGE

from tests.factories import YouTubeChartFactory, YouTubeVideoFactory
from tests.patched_modules.user import not_allowed_run_chart_task


@pytest.mark.django_db
class TestWWMusicVideoChartView:
    def setup_method(self):
        self.url = "/"
        self.view = WWMusicVideoChartView()
        self.view.request = RequestFactory().get(self.url)

    def test_view_get_videos(self, mocker):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_monthly_videos"
        )

        chart = YouTubeChartFactory(chart_type=YouTubeChart.WORLDWIDE, processed=True)
        YouTubeVideoFactory.create_batch(size=VIDEOS_PER_PAGE + 20, chart=chart)

        ctx = self.view.get_context_data()
        assert len(ctx["items"].object_list) == VIDEOS_PER_PAGE
        assert ctx["chart"].name == chart.name
        assert patched_async_task.call_count == 0

    def test_view_get_videos_not_processed(self, mocker):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_monthly_videos"
        )

        YouTubeChartFactory(chart_type=YouTubeChart.WORLDWIDE, processed=False)

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert patched_async_task.call_count == 0

    def test_view_get_videos_start_async_task(self, mocker):
        mocked_thread = mocker.patch("telegram_bots.apps.top_charts.views.videos_views.Thread")

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert mocked_thread.call_count == 2


@pytest.mark.django_db
class TestRUMusicVideoChartView:
    def setup_method(self):
        self.url = "/"
        self.view = RUMusicVideoChartView()
        self.view.request = RequestFactory().get(self.url)

    def test_view_get_videos(self, mocker):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_ru_ua_monthly_videos"
        )

        chart = YouTubeChartFactory(chart_type=YouTubeChart.RU, processed=True)
        YouTubeVideoFactory.create_batch(size=VIDEOS_PER_PAGE + 20, chart=chart)

        ctx = self.view.get_context_data()
        assert len(ctx["items"].object_list) == VIDEOS_PER_PAGE
        assert ctx["chart"].name == chart.name
        assert patched_async_task.call_count == 0

    def test_view_get_videos_not_processed(self, mocker):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_ru_ua_monthly_videos"
        )

        YouTubeChartFactory(chart_type=YouTubeChart.RU, processed=False)

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert patched_async_task.call_count == 0

    def test_view_get_videos_start_async_task(self, mocker):
        mocked_thread = mocker.patch("telegram_bots.apps.top_charts.views.videos_views.Thread")

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert mocked_thread.call_count == 2


@pytest.mark.django_db
class TestTrailersVideosChartView:
    def setup_method(self):
        self.url = "/"
        self.view = TrailersChartView()
        self.view.request = RequestFactory().get(self.url)

    def test_view_get_videos(self, mocker):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_monthly_videos"
        )

        chart = YouTubeChartFactory(chart_type=YouTubeChart.TRAILER, processed=True)
        YouTubeVideoFactory.create_batch(size=VIDEOS_PER_PAGE + 20, chart=chart)

        ctx = self.view.get_context_data()
        assert len(ctx["items"].object_list) == VIDEOS_PER_PAGE
        assert ctx["chart"].name == chart.name
        assert patched_async_task.call_count == 0

    def test_view_get_videos_not_processed(self, mocker):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_monthly_videos"
        )

        YouTubeChartFactory(chart_type=YouTubeChart.TRAILER, processed=False)

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert patched_async_task.call_count == 0

    def test_view_get_videos_start_async_task(self, mocker):
        mocked_thread = mocker.patch("telegram_bots.apps.top_charts.views.videos_views.Thread")

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert mocked_thread.call_count == 2


@pytest.mark.django_db
class TestAllTimeVideosChartView:
    def setup_method(self):
        self.url = "/"
        self.view = ATMusicVideoChartView()
        self.view.request = RequestFactory().get(self.url)

    def test_view_get_videos(self, mocker):
        chart = YouTubeChartFactory(chart_type=YouTubeChart.ALL_TIME, processed=True)

        def patched_last_month_video_chart(*args, **kwargs):
            return chart, False

        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_all_time_videos"
        )
        patched_chart = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.last_month_video_chart",
            side_effect=patched_last_month_video_chart,
        )

        YouTubeVideoFactory.create_batch(size=VIDEOS_PER_PAGE + 20, chart=chart)

        ctx = self.view.get_context_data()
        assert len(ctx["items"].object_list) == VIDEOS_PER_PAGE
        assert ctx["chart"].name == chart.name
        assert patched_chart.call_count == 1
        assert patched_async_task.call_count == 0

    def test_view_get_videos_not_processed(self, mocker):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_fetch_top_all_time_videos"
        )

        YouTubeChartFactory(chart_type=YouTubeChart.ALL_TIME, processed=False)

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert patched_async_task.call_count == 0

    def test_view_get_videos_start_async_task(self, mocker):
        mocked_thread = mocker.patch("telegram_bots.apps.top_charts.views.videos_views.Thread")

        ctx = self.view.get_context_data()
        assert len(ctx["items"]) == 0
        assert "chart" in ctx
        assert mocked_thread.call_count == 2


@pytest.mark.django_db
class TestAPeriodicTaskChartView:
    def setup_method(self):
        self.data = {
            "username": "testname",
            "password": "somepassword",
            "email": "example@example.com",
            "chart_type": YouTubeChart.WORLDWIDE,
        }
        self.url = "/"
        self.view = PeriodicMonthlyChartTaskView()

    def test_post_ok(self, mocker, rf):
        def patched_get_chart_request_parameters(*args, **kwargs):
            return {"data": "some data"}

        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_periodic_fetch_chart_today"
        )
        patched_get_params = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.get_chart_request_parameters",
            side_effect=patched_get_chart_request_parameters,
        )
        patched_check_run_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.is_allowed_run_chart_task"
        )

        response = self.view.post(
            rf.post(self.url, data=self.data, content_type="application/json")
        )

        assert response.status_code == 200
        assert patched_check_run_task.call_count == 1
        assert patched_get_params.call_count == 1
        assert patched_async_task.call_count == 1

    def test_post_not_json(self, mocker, rf):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_periodic_fetch_chart_today"
        )
        patched_check_run_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.is_allowed_run_chart_task"
        )

        response = self.view.post(rf.post(self.url, data=self.data))

        assert response.status_code == 406
        assert patched_check_run_task.call_count == 0
        assert patched_async_task.call_count == 0

    def test_post_wrong_user(self, mocker, rf):
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_periodic_fetch_chart_today"
        )
        patched_check_run_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.is_allowed_run_chart_task",
            side_effect=not_allowed_run_chart_task,
        )

        response = self.view.post(
            rf.post(self.url, data=self.data, content_type="application/json")
        )

        assert response.status_code == 401
        assert patched_check_run_task.call_count == 1
        assert patched_async_task.call_count == 0

    def test_post_no_run_today(self, mocker, rf):
        YouTubeChartFactory(date=tz.now().date())
        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_periodic_fetch_chart_today"
        )
        patched_check_run_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.is_allowed_run_chart_task"
        )
        patched_get_chart_request_parameters = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.get_chart_request_parameters"
        )

        response = self.view.post(
            rf.post(self.url, data=self.data, content_type="application/json")
        )

        assert response.status_code == 406
        assert patched_check_run_task.call_count == 1
        assert patched_get_chart_request_parameters.call_count == 0
        assert patched_async_task.call_count == 0

    def test_post_incorrect_chart_type(self, mocker, rf):
        def patched_get_chart_request_parameters(*args, **kwargs):
            return {}

        patched_async_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.async_periodic_fetch_chart_today"
        )
        patched_get_params = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.get_chart_request_parameters",
            side_effect=patched_get_chart_request_parameters,
        )
        patched_check_run_task = mocker.patch(
            "telegram_bots.apps.top_charts.views.videos_views.is_allowed_run_chart_task"
        )

        response = self.view.post(
            rf.post(self.url, data=self.data, content_type="application/json")
        )

        assert response.status_code == 406
        assert patched_check_run_task.call_count == 1
        assert patched_get_params.call_count == 1
        assert patched_async_task.call_count == 0
