import pytest
from django.utils import timezone as tz
from freezegun import freeze_time
from telegram_bots.apps.top_charts.models import YouTubeChart
from telegram_bots.apps.top_charts.utils import (
    days_ago_iso_end,
    days_ago_iso_start,
    get_or_create_today_video_chart,
    get_youtube_iso_history_date,
    last_month_video_chart,
    month_ago_iso,
)

from tests.factories.charts import YouTubeChartFactory, YouTubeVideoFactory


@freeze_time("2018-12-31")
class TestDatetimeISO:
    def test_month_ago_iso(self):
        result = month_ago_iso()
        assert result == "2018-11-30T00:00:00Z"

    def test_days_ago_iso_start(self):
        result = days_ago_iso_start(days=3)
        assert result == "2018-12-28T00:00:00Z"

    def test_days_ago_iso_end(self):
        result = days_ago_iso_end(days=5)
        assert result == "2018-12-26T23:59:59Z"

    def test_get_youtube_iso_history_date(self):
        assert get_youtube_iso_history_date() == "2006-01-01T00:00:00Z"


@pytest.mark.django_db
class TestLastMonthVideoChart:
    def test_last_month_video_chart_created(self):
        video_chart, created = last_month_video_chart()
        assert created
        assert len(video_chart.name) > 0

    @freeze_time("2018-01-01")
    def test_last_month_video_chart_ok(self):
        today = tz.now().date()
        for i in range(0, 3):
            prev_date = today - tz.timedelta(days=i)
            YouTubeChartFactory(chart_type=YouTubeChart.WORLDWIDE, date=prev_date)
        video_chart, created = last_month_video_chart()
        assert not created
        assert video_chart.date == today

    def test_last_month_video_chart_sort_is_ok(self):
        YouTubeChart.objects.all().delete()
        prev_date = tz.now().date() - tz.timedelta(days=9)
        YouTubeChartFactory(date=prev_date, chart_type=YouTubeChart.ALL_TIME)

        positions_list = [3, 1, 2, 5, 4]
        min_position = 1
        max_position = 5
        new_chart = YouTubeChartFactory(chart_type=YouTubeChart.ALL_TIME)
        for el in positions_list:
            YouTubeVideoFactory(chart=new_chart, position=el)

        video_chart, created = last_month_video_chart(chart_type=YouTubeChart.ALL_TIME)

        assert not created
        videos = video_chart.videos.all()
        assert videos.count() == len(positions_list)
        assert videos.first().position == min_position
        assert videos.last().position == max_position


@pytest.mark.django_db
class TestGetOrCreateVideoChart:
    @freeze_time("2018-01-01")
    def test_get_chart_not_create(self):
        new_chart = YouTubeChartFactory()

        returned_chat, created = get_or_create_today_video_chart(new_chart.chart_type)

        assert returned_chat.pk == new_chart.pk
        assert not created

    @freeze_time("2018-01-01")
    def test_get_chart_create(self):
        returned_chat, created = get_or_create_today_video_chart(YouTubeChart.ALL_TIME)

        assert returned_chat.chart_type == YouTubeChart.ALL_TIME
        assert created
