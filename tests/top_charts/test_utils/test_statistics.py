import datetime as dt
from queue import Queue

from telegram_bots.apps.top_charts.utils.statistics import (
    YTStatistics,
    convert_statistics_to_str,
    get_video_statistics,
    start_video_statistics_thread,
)

from tests.patched_modules.statistics import (
    PatchedThread,
    PatchedYTSearchResponse,
    patched_yt_videos_request_get_200,
    patched_yt_videos_request_get_401,
)


def test_start_video_statistics_thread(mocker):
    patched_thread = mocker.patch(
        "telegram_bots.apps.top_charts.utils.statistics.Thread",
        side_effect=PatchedThread,
    )
    patched_bs = mocker.patch("telegram_bots.apps.top_charts.utils.statistics.BeautifulSoup")

    data = PatchedYTSearchResponse().dict_content
    q = Queue()
    start_video_statistics_thread(data=data, q=q, result=[])

    assert patched_thread.call_count == 1
    assert patched_bs.call_count == 1


class TestGetVideoStatistics:
    yt_statistics = YTStatistics(
        video_id="1234",
        title="Some title",
        watch_url="https://example.com?id=1",
        day=3,
        published_at=dt.datetime.now().date(),
    )

    def test_get_video_statistics_error(self, mocker):
        patched_converter = mocker.patch(
            "telegram_bots.apps.top_charts.utils.statistics.convert_statistics_to_str"
        )
        patched_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.statistics.request_get",
            side_effect=patched_yt_videos_request_get_401,
        )

        q = Queue()
        output = []
        q.put(self.yt_statistics)
        get_video_statistics(q, output)

        assert patched_request.call_count == 1
        assert q.empty()
        assert patched_converter.call_count == 0

    def test_get_video_statistics_ok(self, mocker):
        patched_converter = mocker.patch(
            "telegram_bots.apps.top_charts.utils.statistics.convert_statistics_to_str"
        )
        patched_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.statistics.request_get",
            side_effect=patched_yt_videos_request_get_200,
        )

        q = Queue()
        output = []
        q.put(self.yt_statistics)
        get_video_statistics(q, output)

        assert patched_request.call_count == 1
        assert q.empty()
        assert patched_converter.call_count == 5
        assert len(output) == 1
        assert "url" in output[0]
        assert "statistics" in output[0]


class TestConvertStatisticsToStr:
    def test_convert_to_billions(self):
        value = 2120000000
        value_str = convert_statistics_to_str(value)

        assert value_str == "2.1b"

    def test_convert_to_millions(self):
        value = 123400000
        value_str = convert_statistics_to_str(value)

        assert value_str == "123.4m"

    def test_convert_to_thousands(self):
        value = 123400
        value_str = convert_statistics_to_str(value)

        assert value_str == "123.4k"

    def test_convert_to_number(self):
        value = 999
        value_str = convert_statistics_to_str(value)

        assert value_str == "999"
