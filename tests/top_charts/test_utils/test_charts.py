from queue import Queue

import pytest
from telegram_bots.apps.top_charts.utils.charts import (
    filter_result_by_threshold,
    get_all_time_chart,
    get_chart,
)

from tests.patched_modules.statistics import (
    PatchedQueue,
    patched_yt_search_request_get_200,
    patched_yt_search_request_get_401,
)


@pytest.mark.django_db
class TestDailyChart:
    def test_get_chart_error(self, mocker):
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.Queue",
            side_effect=PatchedQueue,
        )
        patched_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.request_get",
            side_effect=patched_yt_search_request_get_401,
        )
        patched_start_thread = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.start_video_statistics_thread"
        )

        _queue = Queue()
        _queue.put(1)
        get_chart({}, [], _queue)
        assert _queue.empty()
        assert patched_request.call_count == 1
        assert patched_start_thread.call_count == 0

    def test_get_chart_ok(self, mocker):
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.Queue",
            side_effect=PatchedQueue,
        )
        patched_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.request_get",
            side_effect=patched_yt_search_request_get_200,
        )
        patched_start_thread = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.start_video_statistics_thread"
        )

        _queue = Queue()
        _queue.put(1)
        get_chart({}, [], _queue)
        assert _queue.empty()
        assert patched_request.call_count == 1
        assert patched_start_thread.call_count == 1


@pytest.mark.django_db
class TestAllTimeChart:
    def test_all_time_chart_error(self, mocker):
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.Queue",
            side_effect=PatchedQueue,
        )
        patched_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.request_get",
            side_effect=patched_yt_search_request_get_401,
        )
        patched_start_thread = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.start_video_statistics_thread"
        )

        get_all_time_chart({}, [])
        assert patched_request.call_count == 1
        assert patched_start_thread.call_count == 0

    def test_get_chart_ok(self, mocker):
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.Queue",
            side_effect=PatchedQueue,
        )
        patched_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.request_get",
            side_effect=patched_yt_search_request_get_200,
        )
        patched_start_thread = mocker.patch(
            "telegram_bots.apps.top_charts.utils.charts.start_video_statistics_thread"
        )

        get_all_time_chart({}, [])
        assert patched_request.call_count == 1
        assert patched_start_thread.call_count == 1


class TestThresholdFilter:
    def test_threshold_filtered(self):
        output_list = [
            {"name": "Test 1", "statistics": {"views_count": 9}},
            {"name": "Test 2", "statistics": {"views_count": 7}},
            {"name": "Test 3", "statistics": {"views_count": 3}},
        ]
        output_list = filter_result_by_threshold(result=output_list, threshold=5)
        assert len(output_list) == 2

    def test_threshold_not_filtered(self):
        output_list = [
            {"name": "Test 1", "statistics": {"views_count": 9}},
            {"name": "Test 2", "statistics": {"views_count": 7}},
            {"name": "Test 3", "statistics": {"views_count": 5}},
        ]
        output_list = filter_result_by_threshold(result=output_list, threshold=5)
        assert len(output_list) == 3
