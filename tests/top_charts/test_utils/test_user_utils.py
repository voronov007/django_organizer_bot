import pytest
from telegram_bots.apps.top_charts.utils import is_allowed_run_chart_task

from tests.factories import UserFactory


@pytest.mark.django_db
class TestUserUtils:
    def setup_method(self):
        self.password = "Super#123password"  # noqa: S105

    def test_run_chart_task_no_allowed_user(self):
        user = UserFactory(password=self.password)
        assert not is_allowed_run_chart_task(
            {
                "username": user.username,
                "password": self.password,
                "email": user.email,
            }
        )

    def test_run_chart_task_no_incoming_user(self, mocker):
        user = UserFactory(
            username="topchart1",
            email="charts@example.com",
            password=self.password,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.CHART_TASK_EMAIL",
            user.email,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.CHART_TASK_USERNAME",
            user.username,
        )

        assert not is_allowed_run_chart_task({"a": "b", "c": "d"})

    def test_run_chart_task_users_mismatch(self, mocker):
        user1 = UserFactory(username="user1")
        user2 = UserFactory(username="user2", password=self.password)

        def patched_get_user(**_kwargs):
            return user2

        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.CHART_TASK_EMAIL",
            user1.email,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.CHART_TASK_USERNAME",
            user1.username,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.authenticate",
            side_effect=patched_get_user,
        )

        assert not is_allowed_run_chart_task(
            {
                "username": user2.username,
                "email": user2.email,
                "password": self.password,
            }
        )

    def test_run_chart_ok(self, mocker):
        user = UserFactory(
            username="topchart1",
            email="charts@example.com",
            password=self.password,
        )

        def patched_get_user(**_kwargs):
            return user

        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.CHART_TASK_EMAIL",
            user.email,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.CHART_TASK_USERNAME",
            user.username,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.utils.user.authenticate",
            side_effect=patched_get_user,
        )

        assert is_allowed_run_chart_task(
            {
                "username": user.username,
                "email": user.email,
                "password": self.password,
            }
        )
