import datetime as dt

import pytest
from telegram_bots.apps.top_charts.models import YouTubeChart
from telegram_bots.apps.top_charts.utils import clean_up_charts

from tests.factories import YouTubeChartFactory


@pytest.mark.django_db
def test_cleanup():
    init_size = 5
    for i in range(init_size):
        some_date = (dt.datetime.now() - dt.timedelta(days=60 * i)).date()
        YouTubeChartFactory(chart_type=YouTubeChart.RU, date=some_date)

    clean_up_charts(YouTubeChart.RU)
    num_of_charts = YouTubeChart.objects.filter(chart_type=YouTubeChart.RU).count()
    assert num_of_charts == 2
