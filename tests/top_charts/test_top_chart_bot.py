import json

import pytest
from django.test import Client
from telegram_bots.apps.top_charts.models import (
    TelegramCallbackMessage,
    TelegramCallbackUserReaction,
    TopChartTelegramChats,
)


@pytest.mark.django_db
class TestTopChartBotView:
    def setup_method(self):
        self.client = Client()

    def test_plus_one_bot_message(self, mocker):
        callback_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.bots.bot.answer_callback_query"
        )
        msg_update_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.bots.bot.edit_message_reply_markup"
        )
        callback_msg_exists = TelegramCallbackMessage.objects.filter(t_msg_id="3149").first()
        assert not callback_msg_exists

        TopChartTelegramChats.objects.create(chat_id="-1211254429668", is_active=True)
        message_body = b'{"update_id":745002003,\n"callback_query":{"id":"1521210373138914742","from":{"id":456,"is_bot":false,"first_name":"TestName","last_name":"TestSurname","username":"test123","language_code":"en"},"message":{"message_id":3149,"sender_chat":{"id":-1211254429668,"title":"test_title","username":"test_title_channel","type":"channel"},"chat":{"id":-1211254429668,"title":"test_title","username":"test_title_channel","type":"channel"},"date":1697418127,"text":"3. LOBODA \\u2014 \\u0413\\u0420\\u0410\\u0414\\u0423\\u0421 100 (MIX) | \\u041f\\u0420\\u0415\\u041c\'\\u0404\\u0420\\u0410 2023 https://www.youtube.com/watch?v=5T9V9dATCug (\\ud83d\\udcc8 198.8k, \\ud83d\\udc40 1.6m, \\ud83d\\udc4d 25.1k, \\ud83d\\udc4e 0)","entities":[{"offset":0,"length":1,"type":"bold"},{"offset":45,"length":43,"type":"text_link","url":"https://www.youtube.com/watch?v=5T9V9dATCug"}],"reply_markup":{"inline_keyboard":[[{"text":"\\ud83d\\udc4d","callback_data":"+1"},{"text":"\\ud83d\\udc4e","callback_data":"-1"}]]}},"chat_instance":"1992135350389900042","data":"+1"}}'  # noqa: E501, B950
        message = json.loads(message_body)

        response = self.client.post(
            "/webhook/charts/top_chart_bot_007/",
            message,
            content_type="application/json",
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed"
        assert callback_request.call_count == 1
        assert msg_update_request.call_count == 1

        # run 2nd time to revert vote
        response = self.client.post(
            "/webhook/charts/top_chart_bot_007/",
            message,
            content_type="application/json",
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed"
        assert callback_request.call_count == 2
        assert msg_update_request.call_count == 2

        callback_msg_exists = TelegramCallbackMessage.objects.filter(t_msg_id="3149").first()
        assert callback_msg_exists

        callback_user_reaction_exists = TelegramCallbackUserReaction.objects.filter(
            message_id=callback_msg_exists.pk, user_id="456"
        ).first()
        assert callback_user_reaction_exists

    def test_chat_created_during_callback(self, mocker):
        callback_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.bots.bot.answer_callback_query"
        )
        msg_update_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.bots.bot.edit_message_reply_markup"
        )
        new_chat = TopChartTelegramChats.objects.filter(chat_id=str(123)).first()
        assert not new_chat

        message = {
            "update_id": 745002003,
            "callback_query": {
                "id": "1521210373138914742",
                "from": {
                    "id": 456,
                    "is_bot": False,
                    "first_name": "TestName",
                    "last_name": "TestSurname",
                    "username": "test_username",
                    "language_code": "en",
                },
                "message": {
                    "message_id": 3149,
                    "sender_chat": {
                        "id": 123,
                        "title": "test",
                        "username": "test_channel",
                        "type": "channel",
                    },
                    "chat": {
                        "id": 123,
                        "title": "test",
                        "username": "test_channel",
                        "type": "channel",
                    },
                    "date": 1697418127,
                    "text": "3. LOBODA — ГРАДУС 100 (MIX) | ПРЕМ'ЄРА 2023 https://www.youtube.com/watch?v=5T9V9dATCug (📈 198.8k, 👀 1.6m, 👍 25.1k, 👎 0)",  # noqa: E501, B950
                    "entities": [
                        {"offset": 0, "length": 1, "type": "bold"},
                        {
                            "offset": 45,
                            "length": 43,
                            "type": "text_link",
                            "url": "https://www.youtube.com/watch?v=5T9V9dATCug",
                        },
                    ],
                    "reply_markup": {
                        "inline_keyboard": [
                            [
                                {"text": "👍", "callback_data": "+1"},
                                {"text": "👎", "callback_data": "-1"},
                            ]
                        ]
                    },
                },
                "chat_instance": "999",
                "data": "+1",
            },
        }

        response = self.client.post(
            "/webhook/charts/top_chart_bot_007/",
            message,
            content_type="application/json",
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed"
        assert callback_request.call_count == 1
        assert msg_update_request.call_count == 1

        new_chat = TopChartTelegramChats.objects.filter(chat_id=str(123)).first()
        assert new_chat

    def test_is_start_channel_message(self, mocker):
        callback_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.bots.bot.answer_callback_query"
        )
        msg_update_request = mocker.patch(
            "telegram_bots.apps.top_charts.utils.bots.bot.edit_message_reply_markup"
        )

        message = {"channel_post": {"text": "some text", "chat": {"chat_id": "123456"}}}
        response = self.client.post(
            "/webhook/charts/top_chart_bot_007/",
            message,
            content_type="application/json",
        )
        response_data = response.json()
        assert response_data["ok"] == "POST request processed"
        assert callback_request.call_count == 0
        assert msg_update_request.call_count == 0
