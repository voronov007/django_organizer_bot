import pytest
from telegram_bots.apps.top_charts.models import YouTubeVideo
from telegram_bots.apps.top_charts.tasks import (
    async_fetch_top_all_time_videos,
    async_fetch_top_monthly_videos,
    async_fetch_top_ru_ua_monthly_videos,
)
from telegram_bots.constants import VIDEOS_TO_SHOW

from tests.factories import YouTubeChartFactory
from tests.patched_modules.statistics import (
    PatchedQueue,
    patched_get_all_time_chart,
    patched_get_chart_less_threshold,
    patched_get_chart_pass_threshold,
    patched_run_get_chart_worker,
)


@pytest.mark.django_db
class TestAsyncTopMonthlyVideos:
    def test_top_monthly_videos_all_threshold_passed(self, mocker):
        patched_queue = mocker.patch(
            "telegram_bots.apps.top_charts.tasks.Queue",
            side_effect=PatchedQueue,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.tasks.VIDEO_MONTHLY_CHART_START_RANGE",
            5,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.tasks.VIDEO_MONTHLY_CHART_END_RANGE",
            6,
        )
        patched_daily_chart = mocker.patch(
            "telegram_bots.apps.top_charts.tasks.get_chart",
            side_effect=patched_get_chart_pass_threshold,
        )
        video_chart = YouTubeChartFactory()
        async_fetch_top_monthly_videos({}, video_chart.pk)

        new_music_videos = YouTubeVideo.objects.filter(chart__pk=video_chart.pk)
        assert new_music_videos.count() == VIDEOS_TO_SHOW
        assert new_music_videos.first().chart.processed
        assert patched_queue.call_count == 1
        assert patched_daily_chart.call_count == 1

    def test_top_monthly_ru_ua_videos_all_threshold_passed(self, mocker):
        patched_queue = mocker.patch(
            "telegram_bots.apps.top_charts.tasks.Queue",
            side_effect=PatchedQueue,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.tasks.VIDEO_MONTHLY_CHART_START_RANGE",
            5,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.tasks.VIDEO_MONTHLY_CHART_END_RANGE",
            6,
        )
        patched_chart = mocker.patch(
            "telegram_bots.apps.top_charts.tasks.run_get_chart_worker",
            side_effect=patched_run_get_chart_worker,
        )
        video_chart = YouTubeChartFactory()
        async_fetch_top_ru_ua_monthly_videos(video_chart.pk)

        new_music_videos = YouTubeVideo.objects.filter(chart__pk=video_chart.pk)
        assert new_music_videos.count() == VIDEOS_TO_SHOW * 2
        assert new_music_videos.first().chart.processed
        assert patched_queue.call_count == 1
        assert patched_chart.call_count == 2

    def test_top_monthly_videos_not_all_threshold_passed(self, mocker):
        """Test.

        We use a list with views less than threshold but as total list less
        length than needed we still use values from the source list
        """
        patched_queue = mocker.patch(
            "telegram_bots.apps.top_charts.tasks.Queue",
            side_effect=PatchedQueue,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.tasks.VIDEO_MONTHLY_CHART_START_RANGE",
            5,
        )
        mocker.patch(
            "telegram_bots.apps.top_charts.tasks.VIDEO_MONTHLY_CHART_END_RANGE",
            6,
        )
        # threshold < VIDEOS_TO_SHOW, but we will get same results
        patched_daily_chart = mocker.patch(
            "telegram_bots.apps.top_charts.tasks.get_chart",
            side_effect=patched_get_chart_less_threshold,
        )
        video_chart = YouTubeChartFactory()
        async_fetch_top_monthly_videos({}, video_chart.pk)

        new_music_videos = YouTubeVideo.objects.filter(chart__pk=video_chart.pk)
        assert new_music_videos.count() < VIDEOS_TO_SHOW
        assert new_music_videos.first().chart.processed
        assert patched_queue.call_count == 1
        assert patched_daily_chart.call_count == 1


@pytest.mark.django_db
class TestAsyncTopAllTimeVideos:
    def test_top_all_time_videos(self, mocker):
        patched_all_time_chart = mocker.patch(
            "telegram_bots.apps.top_charts.tasks.get_all_time_chart",
            side_effect=patched_get_all_time_chart,
        )
        video_chart = YouTubeChartFactory()
        async_fetch_top_all_time_videos({}, video_chart.pk)

        new_music_videos = YouTubeVideo.objects.filter(chart__pk=video_chart.pk)
        assert new_music_videos.count() == VIDEOS_TO_SHOW
        assert new_music_videos.first().chart.processed
        assert patched_all_time_chart.call_count == 1
