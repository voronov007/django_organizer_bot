#!/bin/sh

#: Configure application
sleep 3
poetry run python /code/src/manage.py collectstatic --noinput
poetry run python /code/src/manage.py migrate --noinput

#: Lastly, Run webserver
if [ $LOCAL_DEV == "YES" ]
then
    poetry run python /code/src/manage.py runserver 0.0.0.0:$SERVER_PORT
else
    poetry run python /code/src/manage.py runserver 0.0.0.0:$SERVER_PORT
    # uwsgi --ini /code/docker/uwsgi.ini
fi
